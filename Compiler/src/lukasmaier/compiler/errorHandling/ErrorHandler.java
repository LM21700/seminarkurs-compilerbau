package lukasmaier.compiler.errorHandling;

import lukasmaier.compiler.errorHandling.exception.TokenDoesNotExistInTokenStorageException;
import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenStorage;
import lukasmaier.compiler.lexer.TokenType;

/**
 * Class for handling compiler errors. The compiler exits after printing a error message.
 * @author lukasmaier
 *
 */
public class ErrorHandler
{
	private TokenStorage tokens;
	
	/**
	 * Sets the provided TokenStorage as context for errors 
	 * @param tokens
	 */
	public void setTokenStorage(TokenStorage tokens)
	{
		this.tokens = tokens;
	}
	
	/**
	 * Prints an error message with the token and line where the error occurred.
	 * Then it exits with the provided error code
	 * @param error error message
	 * @param t token where the error occurred
	 * @param exitCode exit code with the compiler will exit
	 */
	public void printError(String error, Token t, ExitCodes exitCode)
	{
		printError(error + "\n" + "at token " + t.toString() + "\nat line " + getLineForToken(t), exitCode);

	}

	/**
	 * Prints an error message and exits the compiler with the provided exit code
	 * @param error error message
	 * @param exitCode exit code with that the compiler will exit
	 */
	public void printError(String error, ExitCodes exitCode)
	{
		System.err.print("Error: " + error + "\n");
		System.exit(exitCode.getExitCode());
	}
	
	/**
	 * Does not print an error message, but an normal message (or comment).
	 * Then the compiler will exit with the provided error message
	 * @param comment message with that the compiler will exit
	 * @param exitCode exit code with the compiler will exit
	 */
	public void exitWithComment(String comment, ExitCodes exitCode)
	{
		System.out.println(comment);
		System.exit(exitCode.getExitCode());

	}
	
	/**
	 * Prints an error message with the line number where the error occurred.
	 * Then the compiler will exit with the provided error message.
	 * @param error error message
	 * @param lineNumber line number where the error occurred
	 * @param exitCode exit code with that the compiler will exit
	 */
	public void printError(String error, int lineNumber, ExitCodes exitCode)
	{
		printError(error + "\n" + "line: " + lineNumber, exitCode);
	}
	
	/**
	 * Searches for the line in which the provided token is and returns the line number
	 * @param t token for which the line number should be searched
	 * @return line number of the provided token
	 */
	public int getLineForToken(Token t)
	{
		int line = 1;
		for (int i = 0; i < tokens.size(); i++)
		{
			if (t == tokens.getTokenAt(i))
			{
				return line;
			} else if (tokens.getTokenAt(i).getType() == TokenType.NEW_LINE)
			{
				line++;
			}
		}
		
		throw new TokenDoesNotExistInTokenStorageException();
	}
	
}
