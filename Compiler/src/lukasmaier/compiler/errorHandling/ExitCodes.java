package lukasmaier.compiler.errorHandling;

/**
 * Provides exit codes for different termination scenarios
 * of the compiler
 * @author lukasmaier
 *
 */
public enum ExitCodes
{
	SUCCESS(0),
	UNDEFINED(4),
	ILLEGAL_ARGUEMENTS(5),
	ILLEGAL_FILE_PATH(6),
	LEXER_ERROR(7),
	SYNTACTIC_ANALIZER_ERROR(8),
	SEMANTIC_ANALIZER_ERROR(9),
	CODE_GENERATION_ERROR(10),
	IO_Error(11);
	
	private int exitCode;
	
	ExitCodes(int exitCode)
	{
		this.exitCode = exitCode;
	}
	
	/**
	 * extracts exit code from enum element
	 * @return exit code
	 */
	public int getExitCode()
	{
		return exitCode;
	}
}
