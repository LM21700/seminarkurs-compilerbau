package lukasmaier.compiler.errorHandling.exception;

/**
 * Exception that is be thrown when Token is not found
 * in the TokenStorage where the Token is searched in
 * @author lukasmaier
 *
 */
public class TokenDoesNotExistInTokenStorageException extends RuntimeException {

	private static final long serialVersionUID = -2856079638687173745L;

	public TokenDoesNotExistInTokenStorageException()
	{
	}

	public TokenDoesNotExistInTokenStorageException(String arg0) 
	{
		super(arg0);
	}

	public TokenDoesNotExistInTokenStorageException(Throwable arg0)
	{
		super(arg0);
	}

	public TokenDoesNotExistInTokenStorageException(String arg0, Throwable arg1)
	{
		super(arg0, arg1);
	}

	public TokenDoesNotExistInTokenStorageException(String arg0, Throwable arg1, boolean arg2, boolean arg3) 
	{
		super(arg0, arg1, arg2, arg3);
	}

}
