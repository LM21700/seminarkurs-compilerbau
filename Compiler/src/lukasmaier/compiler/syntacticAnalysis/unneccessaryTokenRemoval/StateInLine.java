package lukasmaier.compiler.syntacticAnalysis.unneccessaryTokenRemoval;

import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenType;

/**
 * Class for all states that are used internally by
 * the unnecessary token remover to operate.
 * @author lukasmaier
 *
 */
public class StateInLine extends State
{

	/**
	 * Creates a new state object
	 * @param parent UnnecessaryTokenRemover object on which this state operates
	 */
	public StateInLine(UnnecessaryTokenRemover parent)
	{
		super(parent);
	}

	/**
	 * Called by the parent object. Performs the action of the
	 * state depending on the provided token.
	 * @param t token which the state object is going to analyze
	 */
	@Override
	public void operate(Token t) 
	{
		if (t.getType() == TokenType.NEW_LINE)
		{
			parent.setCurrentStateNewLine();
		} else if (t.getType() == TokenType.BLANK || t.getType() == TokenType.TABULATOR || t.getType() == TokenType.COMMENT)
		{
			parent.removeToken(t);
		}
	}

}
