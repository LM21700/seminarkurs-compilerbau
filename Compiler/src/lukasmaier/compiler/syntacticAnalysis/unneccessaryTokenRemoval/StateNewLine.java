package lukasmaier.compiler.syntacticAnalysis.unneccessaryTokenRemoval;

import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenType;

public class StateNewLine extends State 
{

	/**
	 * Creates a new state object
	 * @param parent UnnecessaryTokenRemover object on which this state operates
	 */
	public StateNewLine(UnnecessaryTokenRemover parent)
	{
		super(parent);
	}

	/**
	 * Called by the parent object. Performs the action of the
	 * state depending on the provided token.
	 * @param t token which the state object is going to analyze
	 */
	@Override
	public void operate(Token t)
	{
		if (t.getType() != TokenType.BLANK && t.getType() != TokenType.TABULATOR && 
				t.getType() != TokenType.NEW_LINE && t.getType() != TokenType.COMMENT)
		{
			parent.setCurrentStateInLine();
		}
	}

}
