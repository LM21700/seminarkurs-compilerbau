package lukasmaier.compiler.syntacticAnalysis.unneccessaryTokenRemoval;

import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenStorage;
import lukasmaier.compiler.lexer.TokenType;

/**
 * Class to remove Tokens from TokenStorage that don't
 * matter for syntactical and semantical analysis.
 * @author lukasmaier
 *
 */
public class UnnecessaryTokenRemover
{

	private TokenStorage original;
	private TokenStorage removed = new TokenStorage();
	private State currentState;
	private StateNewLine stateNewLine;
	private StateInLine stateInLine;
	
	/**
	 * Creates a new UnnecessaryTokenRemover object using a TokenStorage.
	 * @param original TokenStorage from which the unnecessary tokens should be removed
	 */
	public UnnecessaryTokenRemover(TokenStorage original)
	{
		this.original = original;
		stateNewLine = new StateNewLine(this);
		stateInLine = new StateInLine(this);
	}
	
	/**
	 * starts the unnecessary token remover
	 * 
	 * @return a new TokenStorage object from which all 
	 * tokens that are unnecessary for syntactical and semantical analysis
	 * are removed
	 */
	public TokenStorage removeUnnecessaryTokens()
	{
		removed = original.clone();
		
		// remove all comments
		for (int i = 0; i< original.size(); i++)
		{
			Token t = original.getTokenAt(i);
			if (t.getType() == TokenType.COMMENT)
			{
				removed.removeToken(t);
			}
		}
		
		// remove unnecessary whitespace tokens
		currentState = stateInLine;
		for (int i = 0; i < original.size(); i++)
		{
			Token t = original.getTokenAt(i);
			currentState.operate(t);
		}
		
		removed.addToken(new Token("\n", TokenType.NEW_LINE));		
		return removed;
	}
	
	/**
	 * Removes the provided Token from the cloned original TokenStorage.
	 * @param t token which should be removed
	 */
	public void removeToken(Token t)
	{
		removed.removeToken(t);
	}
	
	/**
	 * Changes the internal state of the unnecessary token remover
	 * to "StateNewLine".
	 */
	public void setCurrentStateNewLine()
	{
		currentState = stateNewLine;
	}
	
	/**
	 * Changes the internal state of the unnecessary token remover
	 * to "StateInLine".
	 */
	public void setCurrentStateInLine()
	{
		currentState = stateInLine;
	}

}
