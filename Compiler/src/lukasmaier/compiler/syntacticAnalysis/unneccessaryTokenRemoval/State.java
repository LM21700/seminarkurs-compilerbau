package lukasmaier.compiler.syntacticAnalysis.unneccessaryTokenRemoval;

import lukasmaier.compiler.lexer.Token;

/**
 * Super class for all states that are used internally by
 * the unnecessary token remover to operate.
 * @author lukasmaier
 *
 */
public abstract class State
{
	protected UnnecessaryTokenRemover parent;
	
	/**
	 * Creates a new state object
	 * @param parent UnnecessaryTokenRemover object on which this state operates
	 */
	public State(UnnecessaryTokenRemover parent)
	{
		this.parent = parent;
	}
	
	/**
	 * Called by the parent object. Performs the action of the
	 * state depending on the provided token.
	 * @param t token which the state object is going to analyze
	 */
	public abstract void operate(Token t);
}
