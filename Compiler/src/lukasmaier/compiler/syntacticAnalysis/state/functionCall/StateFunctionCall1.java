package lukasmaier.compiler.syntacticAnalysis.state.functionCall;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElement;
import lukasmaier.compiler.syntacticAnalysis.state.State;

/**
 * State class which is used by the syntactical
 * analyzer internally to analyze the source code in form of
 * tokens and convert it to an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class StateFunctionCall1 extends State
{

	/**
	 * Creates a new state object
	 * @param parent
	 */
	public StateFunctionCall1(AbstractSyntaxTree parent)
	{
		super(parent);
	}

	// IMPLEMENT operate(Token) in class StateFunctionCall1
	/**
	 * Performs the action of the state depending
	 * on the provided token
	 * @param t token that the state is going to analyze
	 */
	@Override
	public void operate(Token t)
	{
		parent.getErrorHandler().printError("Not yet implemented", t, ExitCodes.SYNTACTIC_ANALIZER_ERROR);
	}
	
	// IMPLEMENT parseArguements() in class StateFunctionCall1 
	private AbstractSyntaxTreeElement parseArguements()
	{
		return null;
	}

}
