package lukasmaier.compiler.syntacticAnalysis.state.assignment;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.state.State;
import lukasmaier.compiler.syntacticAnalysis.state.States;

/**
 * State class which is used by the syntactical
 * analyzer internally to analyze the source code in form of
 * tokens and convert it to an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class StateAssignment1 extends State
{

	/**
	 * Creates a new state object
	 * @param parent
	 */
	public StateAssignment1(AbstractSyntaxTree parent)
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * on the provided token
	 * @param t token that the state is going to analyze
	 */
	@Override
	public void operate(Token t)
	{
		if (t.getType() == TokenType.NEW_LINE)
		{
			parent.getErrorHandler().printError("Expression expected. Got line break", t, ExitCodes.SYNTACTIC_ANALIZER_ERROR);
		} else
		{
			//parent.clearTokenBuffer();
			parent.addTokenToBuffer(t);
			parent.setCurrentState(States.ASSIGNMENT2.getState());
			parent.nextToken();
		}

	}

}
