package lukasmaier.compiler.syntacticAnalysis.state.assignment;

import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenStorage;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.syntacticAnalysis.ParseExpression;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElement;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElementType;
import lukasmaier.compiler.syntacticAnalysis.state.State;
import lukasmaier.compiler.syntacticAnalysis.state.States;

/**
 * State class which is used by the syntactical
 * analyzer internally to analyze the source code in form of
 * tokens and convert it to an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class StateAssignment2 extends State
{

	/**
	 * Creates a new state object
	 * @param parent
	 */
	public StateAssignment2(AbstractSyntaxTree parent)
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * on the provided token
	 * @param t token that the state is going to analyze
	 */
	@Override
	public void operate(Token t)
	{
		if (t.getType() == TokenType.NEW_LINE)
		{
			AbstractSyntaxTreeElement newElement = ParseExpression.parseExpression(new TokenStorage(parent.getTokenBuffer()), parent.getErrorHandler());
			newElement.setParent(parent.getCurrentElement());
			parent.addElementToAbstractSyntaxTree(newElement);
			parent.clearTokenBuffer();
			parent.goBackToLastElementOfType(AbstractSyntaxTreeElementType.BLOCK);
			parent.setCurrentState(States.START.getState());
			parent.nextToken();
		} else
		{
			parent.addTokenToBuffer(t);
			parent.nextToken();
		}
	}

}
