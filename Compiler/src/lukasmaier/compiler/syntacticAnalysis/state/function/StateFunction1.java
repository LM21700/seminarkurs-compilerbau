package lukasmaier.compiler.syntacticAnalysis.state.function;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElement;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElementType;
import lukasmaier.compiler.syntacticAnalysis.state.State;
import lukasmaier.compiler.syntacticAnalysis.state.States;

/**
 * State class which is used by the syntactical
 * analyzer internally to analyze the source code in form of
 * tokens and convert it to an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class StateFunction1 extends State
{

	/**
	 * Creates a new state object
	 * @param parent
	 */
	public StateFunction1(AbstractSyntaxTree parent)
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * on the provided token
	 * @param t token that the state is going to analyze
	 */
	@Override
	public void operate(Token t)
	{
		parent.addElementToAbstractSyntaxTree(new AbstractSyntaxTreeElement(
				AbstractSyntaxTreeElementType.FUNCTION, null, parent.getCurrentElement(), null));

		if (t.getType() == TokenType.IDENTIFIER)
		{
			parent.setCurrentState(States.FUNCTION2.getState());
			parent.addElementToAbstractSyntaxTree(new AbstractSyntaxTreeElement(
					AbstractSyntaxTreeElementType.FUNCTION_NAME, t.getContent(), parent.getCurrentElement(), null));
			parent.goOneElementBack();
			parent.nextToken();
		} else
		{
			parent.getErrorHandler().printError("Expected function name.", t, ExitCodes.SYNTACTIC_ANALIZER_ERROR);
		}
	}

}
