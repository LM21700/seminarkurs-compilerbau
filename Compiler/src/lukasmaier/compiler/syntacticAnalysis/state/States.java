package lukasmaier.compiler.syntacticAnalysis.state;

import java.lang.reflect.InvocationTargetException;

import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.state.assignment.StateAssignment1;
import lukasmaier.compiler.syntacticAnalysis.state.assignment.StateAssignment2;
import lukasmaier.compiler.syntacticAnalysis.state.controlStructures.StateIf1;
import lukasmaier.compiler.syntacticAnalysis.state.controlStructures.StateWhile1;
import lukasmaier.compiler.syntacticAnalysis.state.function.StateFunction1;
import lukasmaier.compiler.syntacticAnalysis.state.function.StateFunction2;
import lukasmaier.compiler.syntacticAnalysis.state.function.StateFunction3;
import lukasmaier.compiler.syntacticAnalysis.state.function.StateFunction4;
import lukasmaier.compiler.syntacticAnalysis.state.function.StateFunction5;
import lukasmaier.compiler.syntacticAnalysis.state.function.StateFunction6;
import lukasmaier.compiler.syntacticAnalysis.state.function.StateFunction7;
import lukasmaier.compiler.syntacticAnalysis.state.function.StateFunction8;
import lukasmaier.compiler.syntacticAnalysis.state.function.StateFunction9;
import lukasmaier.compiler.syntacticAnalysis.state.functionCall.StateFunctionCall1;
import lukasmaier.compiler.syntacticAnalysis.state.imports.StateImport1;
import lukasmaier.compiler.syntacticAnalysis.state.imports.StateImport2;
import lukasmaier.compiler.syntacticAnalysis.state.indentation.StateCheckIndentation1;
import lukasmaier.compiler.syntacticAnalysis.state.indentation.StateCountIndentation1;
import lukasmaier.compiler.syntacticAnalysis.state.indentation.StateCountIndentation2;
import lukasmaier.compiler.syntacticAnalysis.state.misc.StateBeginsWithIdentifier;
import lukasmaier.compiler.syntacticAnalysis.state.misc.StateKeyword;
import lukasmaier.compiler.syntacticAnalysis.state.variableDeclaration.StateVariableDeclaration;

/**
 * Contains an enumeration of all states that are used by the
 * syntactical analyzer to operate.
 * @author lukasmaier
 *
 */
public enum States
{
	START(StateStart.class),
	IMPORT1(StateImport1.class),
	IMPORT2(StateImport2.class),
	FUNCTION1(StateFunction1.class),
	FUNCTION2(StateFunction2.class),
	FUNCTION3(StateFunction3.class),
	FUNCTION4(StateFunction4.class),
	FUNCTION5(StateFunction5.class),
	FUNCTION6(StateFunction6.class),
	FUNCTION7(StateFunction7.class),
	FUNCTION8(StateFunction8.class),
	FUNCTION9(StateFunction9.class),
	COUNT_INDENTATION1(StateCountIndentation1.class),
	COUNT_INDENTATION2(StateCountIndentation2.class),
	CHECK_INDENTATION1(StateCheckIndentation1.class),
	IF1(StateIf1.class),
	WHILE1(StateWhile1.class),
	FUNCTION_CALL1(StateFunctionCall1.class),
	ASSIGNMENT1(StateAssignment1.class),
	ASSIGNMENT2(StateAssignment2.class),
	BEGINS_WITH_IDENTIFIER(StateBeginsWithIdentifier.class),
	KEYWORD(StateKeyword.class),
	VARIABLE_DECLARATION(StateVariableDeclaration.class);
	
	private State state;
	private Class<? extends State> c;
	
	private States(Class<? extends State> c) 
	{
		this.c = c;
	}
	
	/**
	 * Creates a new instance of the class it represents, if no exists and returns it.
	 * @return an instance of the class it represents
	 */
	public State getState()
	{
		if (state == null)
		{
			try 
			{
				state = (State) c.getConstructor(AbstractSyntaxTree.class).newInstance(AbstractSyntaxTree.getInstance());
			} catch (NoSuchMethodException | SecurityException | InstantiationException |
						IllegalAccessException | IllegalArgumentException | InvocationTargetException e) 
			{
				e.printStackTrace();
			}
		}
		return state;
	}
}
