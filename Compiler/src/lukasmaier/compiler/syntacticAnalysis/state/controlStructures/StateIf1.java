package lukasmaier.compiler.syntacticAnalysis.state.controlStructures;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.state.State;

/**
 * State class which is used by the syntactical
 * analyzer internally to analyze the source code in form of
 * tokens and convert it to an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class StateIf1 extends State
{

	/**
	 * Creates a new state object
	 * @param parent
	 */
	public StateIf1(AbstractSyntaxTree parent)
	{
		super(parent);
	}

	// IMPLEMENT operate(Token) in class StateIf1
	/**
	 * Performs the action of the state depending
	 * on the provided token
	 * @param t token that the state is going to analyze
	 */
	@Override
	public void operate(Token t)
	{
		if (t.getType() == TokenType.OPEN_PARENTHESIS)
		{
			
		} else
		{
			parent.getErrorHandler().printError("Expected '('", t, ExitCodes.SYNTACTIC_ANALIZER_ERROR);
		}
	}

}
