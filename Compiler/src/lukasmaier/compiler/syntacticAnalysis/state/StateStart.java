package lukasmaier.compiler.syntacticAnalysis.state;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.util.TokenUtil;

/**
 * State class which is used by the syntactical
 * analyzer internally to analyze the source code in form of
 * tokens and convert it to an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class StateStart extends State
{

	/**
	 * Creates a new state object
	 * @param parent
	 */
	public StateStart(AbstractSyntaxTree parent)
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * on the provided token
	 * @param t token that the state is going to analyze
	 */
	@Override
	public void operate(Token t)
	{
		if (t.getType() == TokenType.KEYWORD)
		{
			parent.setCurrentState(States.KEYWORD.getState());
		}else if (t.getType() == TokenType.NEW_LINE)
		{
			parent.nextToken();
		} else if (TokenUtil.isIndentationCharakter(t))
		{
			parent.clearTokenBuffer();
			parent.addTokenToBuffer(t);
			parent.setCurrentState(States.COUNT_INDENTATION1.getState());
			parent.nextToken();
		} else if (t.getType() == TokenType.IDENTIFIER)
		{
			parent.setCurrentState(States.BEGINS_WITH_IDENTIFIER.getState());
			parent.nextToken();
			
		} else
		{
			parent.getErrorHandler().printError("Unknown Statement.", t, ExitCodes.SYNTACTIC_ANALIZER_ERROR);
		}
		
	}

}
