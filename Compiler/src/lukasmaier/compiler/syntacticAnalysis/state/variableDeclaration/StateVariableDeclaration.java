package lukasmaier.compiler.syntacticAnalysis.state.variableDeclaration;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElement;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElementType;
import lukasmaier.compiler.syntacticAnalysis.state.State;
import lukasmaier.compiler.syntacticAnalysis.state.States;

/**
 * State class which is used by the syntactical
 * analyzer internally to analyze the source code in form of
 * tokens and convert it to an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class StateVariableDeclaration extends State
{

	/**
	 * Creates a new state object
	 * @param parent
	 */
	public StateVariableDeclaration(AbstractSyntaxTree parent)
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * on the provided token
	 * @param t token that the state is going to analyze
	 */
	@Override
	public void operate(Token t)
	{
		if (t.getType() == TokenType.ASSIGNMENT)
		{
			parent.setCurrentState(States.ASSIGNMENT1.getState());
			parent.goBackToLastElementOfType(AbstractSyntaxTreeElementType.BLOCK);
			parent.addElementToAbstractSyntaxTree(new AbstractSyntaxTreeElement(AbstractSyntaxTreeElementType.ASSIGNMENT, null, parent.getCurrentElement()));
			parent.addElementToAbstractSyntaxTree(new AbstractSyntaxTreeElement(AbstractSyntaxTreeElementType.VARIABLE, parent.getPreviousToken().getContent(), parent.getCurrentElement()));
			parent.goOneElementBack();
			parent.nextToken();
		} else if (t.getType() == TokenType.NEW_LINE)
		{
			parent.goBackToLastElementOfType(AbstractSyntaxTreeElementType.BLOCK);
			parent.setCurrentState(States.CHECK_INDENTATION1.getState());
			parent.nextToken();
		} else
		{
			parent.getErrorHandler().printError("Unknown statement.", t, ExitCodes.SYNTACTIC_ANALIZER_ERROR);
		}
	}

}
