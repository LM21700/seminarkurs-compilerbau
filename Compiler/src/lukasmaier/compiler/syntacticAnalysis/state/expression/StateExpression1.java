package lukasmaier.compiler.syntacticAnalysis.state.expression;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElement;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElementType;
import lukasmaier.compiler.syntacticAnalysis.state.State;

/**
 * State class which is used by the syntactical
 * analyzer internally to analyze the source code in form of
 * tokens and convert it to an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class StateExpression1 extends State
{

	/**
	 * Creates a new state object
	 * @param parent
	 */
	public StateExpression1(AbstractSyntaxTree parent)
	{
		super(parent);
	}

	// IMPLEMENT operate(Token) in class StateExpression1
	/**
	 * Performs the action of the state depending
	 * on the provided token
	 * @param t token that the state is going to analyze
	 */
	@Override
	public void operate(Token t)
	{
		if (t.getType() == TokenType.OPEN_PARENTHESIS)
		{
			
		} else if (t.getType() == TokenType.ASSIGNMENT)
		{
			
		} else if (t.getType() == TokenType.IDENTIFIER)
		{
			parent.addElementToAbstractSyntaxTree(new AbstractSyntaxTreeElement(AbstractSyntaxTreeElementType.VARIABLE_DECLARATION,
					null, parent.getCurrentElement()));
			parent.addElementToAbstractSyntaxTree(new AbstractSyntaxTreeElement(AbstractSyntaxTreeElementType.VARIBALE_TYPE,
					parent.getPreviousToken().getContent(), parent.getCurrentElement()));
			parent.goOneElementBack();
			parent.addElementToAbstractSyntaxTree(new AbstractSyntaxTreeElement(AbstractSyntaxTreeElementType.VARIABLE,
					t.getContent(), parent.getCurrentElement()));
		} else 
		{
			parent.getErrorHandler().printError("Expected either '=', '(' or variable name", t, ExitCodes.SYNTACTIC_ANALIZER_ERROR);
		}
	}

}
