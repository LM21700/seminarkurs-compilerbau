package lukasmaier.compiler.syntacticAnalysis.state.indentation;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElement;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElementType;
import lukasmaier.compiler.syntacticAnalysis.state.State;
import lukasmaier.compiler.syntacticAnalysis.state.States;
import lukasmaier.util.TokenUtil;

/**
 * State class which is used by the syntactical
 * analyzer internally to analyze the source code in form of
 * tokens and convert it to an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class StateCountIndentation2 extends State
{

	/**
	 * Creates a new state object
	 * @param parent
	 */
	public StateCountIndentation2(AbstractSyntaxTree parent)
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * on the provided token
	 * @param t token that the state is going to analyze
	 */
	@Override
	public void operate(Token t)
	{
		if (TokenUtil.isIndentationCharakter(t))
		{
			parent.addTokenToBuffer(t);
			parent.nextToken();
		} else if (t.getType() == TokenType.NEW_LINE)
		{
			parent.clearTokenBuffer();
			parent.setCurrentState(States.COUNT_INDENTATION1.getState());
			parent.nextToken();
		} else
		{
			if (isParentIndentSmaller(parent.getTokenBuffer().size()))
			{
				parent.addElementToAbstractSyntaxTree(new AbstractSyntaxTreeElement(AbstractSyntaxTreeElementType.BLOCK,
						null, parent.getCurrentElement(), new Integer(parent.getTokenBuffer().size())));
			} else
			{
				goBackToBlockOfFittingIndentation();
			}
			parent.clearTokenBuffer();
			parent.setCurrentState(States.START.getState());
		}
	}
	
	// IMPLEMENT goBacKToBlockOfFittingIndentation in class StateCountIndentation2
	private void goBackToBlockOfFittingIndentation()
	{
		
		parent.getErrorHandler().printError("Not yet implemented", ExitCodes.SYNTACTIC_ANALIZER_ERROR);	
	}

	private boolean isParentIndentSmaller(int newIndentCount)
	{
		AbstractSyntaxTreeElement current = parent.getCurrentElement();
		while (true)
		{
			if (current.getIdentation() != null && current.getIdentation() < newIndentCount)
			{
				return true;
			} else if (current.getIdentation() != null &&current.getIdentation() > newIndentCount)
			{
				return false;
			} 
			current = current.getParent();
		}
	}
	
}
