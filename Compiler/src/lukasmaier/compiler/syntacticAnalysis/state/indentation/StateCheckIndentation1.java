package lukasmaier.compiler.syntacticAnalysis.state.indentation;

import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.state.State;
import lukasmaier.compiler.syntacticAnalysis.state.States;
import lukasmaier.util.TokenUtil;

/**
 * State class which is used by the syntactical
 * analyzer internally to analyze the source code in form of
 * tokens and convert it to an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class StateCheckIndentation1 extends State
{

	/**
	 * Creates a new state object
	 * @param parent
	 */
	public StateCheckIndentation1(AbstractSyntaxTree parent)
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * on the provided token
	 * @param t token that the state is going to analyze
	 */
	@Override
	public void operate(Token t)
	{
		if (t.getType() == TokenType.NEW_LINE)
		{
			parent.clearTokenBuffer();
			//parent.setCurrentState(States.START.getState());
			parent.nextToken();
		} else if (TokenUtil.isIndentationCharakter(t))
		{
			parent.addTokenToBuffer(t);
			parent.nextToken();
		} else
		{
			checkIndentation(parent.getTokenBuffer().size());
			parent.clearTokenBuffer();
			parent.setCurrentState(States.START.getState());
		}
	}

	private void checkIndentation(int size)
	{
		while (true)
		{
			Integer indent = parent.getCurrentElement().getIdentation();
			if (indent == null || size < indent)
			{
				parent.goOneElementBack();
				continue;
			} else
			{
				break;
			}
		}
	}

}
