package lukasmaier.compiler.syntacticAnalysis.state.indentation;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.state.State;
import lukasmaier.compiler.syntacticAnalysis.state.States;
import lukasmaier.util.TokenUtil;

/**
 * State class which is used by the syntactical
 * analyzer internally to analyze the source code in form of
 * tokens and convert it to an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class StateCountIndentation1 extends State
{

	/**
	 * Creates a new state object
	 * @param parent
	 */
	public StateCountIndentation1(AbstractSyntaxTree parent)
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * on the provided token
	 * @param t token that the state is going to analyze
	 */
	@Override
	public void operate(Token t)
	{
		if (TokenUtil.isIndentationCharakter(t))
		{
			parent.clearTokenBuffer();
			parent.addTokenToBuffer(t);
			parent.setCurrentState(States.COUNT_INDENTATION2.getState());
			parent.nextToken();
		} else if (t.getType() == TokenType.NEW_LINE)
		{
			parent.clearTokenBuffer();
			parent.nextToken();
		} else
		{
			
			parent.getErrorHandler().printError("Indentation expected", t, ExitCodes.SYNTACTIC_ANALIZER_ERROR);
		}
	}

}
