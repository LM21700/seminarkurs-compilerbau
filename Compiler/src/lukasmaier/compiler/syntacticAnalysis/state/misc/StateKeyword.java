package lukasmaier.compiler.syntacticAnalysis.state.misc;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.state.State;
import lukasmaier.compiler.syntacticAnalysis.state.States;

/**
 * State class which is used by the syntactical
 * analyzer internally to analyze the source code in form of
 * tokens and convert it to an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class StateKeyword extends State
{

	/**
	 * Creates a new state object
	 * @param parent
	 */
	public StateKeyword(AbstractSyntaxTree parent)
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * on the provided token
	 * @param t token that the state is going to analyze
	 */
	@Override
	public void operate(Token t)
	{
		if (parent.isBeginOfLine())
		{
			if (t.getContent().toLowerCase().equals("import") && parent.isBeginOfLine())
			{
				parent.setCurrentState(States.IMPORT1.getState());
				parent.nextToken();
			} else if (t.getContent().toLowerCase().equals("function") && parent.isBeginOfLine())
			{
				parent.setCurrentState(States.FUNCTION1.getState());
				parent.nextToken();
			}
		} else if (t.getContent().equals("while") && parent.isInFunctionBlock())
		{
			parent.setCurrentState(States.WHILE1.getState());
			parent.nextToken();
		} else if (t.getContent().toLowerCase().equals("if") && parent.isInFunctionBlock())
		{
			parent.setCurrentState(States.IF1.getState());
			parent.nextToken();
		} else
		{
			parent.getErrorHandler().printError("Unknown Statement", t, ExitCodes.SYNTACTIC_ANALIZER_ERROR);
		}
	}

}
