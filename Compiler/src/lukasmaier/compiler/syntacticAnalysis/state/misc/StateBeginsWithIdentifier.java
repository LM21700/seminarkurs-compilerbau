package lukasmaier.compiler.syntacticAnalysis.state.misc;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElement;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElementType;
import lukasmaier.compiler.syntacticAnalysis.state.State;
import lukasmaier.compiler.syntacticAnalysis.state.States;

/**
 * State class which is used by the syntactical
 * analyzer internally to analyze the source code in form of
 * tokens and convert it to an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class StateBeginsWithIdentifier extends State
{

	/**
	 * Creates a new state object
	 * @param parent
	 */
	public StateBeginsWithIdentifier(AbstractSyntaxTree parent)
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * on the provided token
	 * @param t token that the state is going to analyze
	 */
	@Override
	public void operate(Token t)
	{
		if (t.getType() == TokenType.OPEN_PARENTHESIS)
		{
			parent.setCurrentState(States.FUNCTION_CALL1.getState());
			parent.nextToken(); // current Token is now Token after Token of type OPEN_PARENTHESIS
			
		} else if (t.getType() == TokenType.ASSIGNMENT)
		{
			parent.setCurrentState(States.ASSIGNMENT1.getState());
			parent.nextToken(); // current Token is now Token after Token of type ASSIGNMENT
			
		} else if (t.getType() == TokenType.IDENTIFIER)
		{
			parent.addElementToAbstractSyntaxTree(new AbstractSyntaxTreeElement(AbstractSyntaxTreeElementType.VARIABLE_DECLARATION, null, parent.getCurrentElement()));
			parent.addElementToAbstractSyntaxTree(new AbstractSyntaxTreeElement(AbstractSyntaxTreeElementType.VARIBALE_TYPE, parent.getPreviousToken().getContent(), parent.getCurrentElement()));
			parent.goOneElementBack();
			parent.addElementToAbstractSyntaxTree(new AbstractSyntaxTreeElement(AbstractSyntaxTreeElementType.VARIABLE, t.getContent(), parent.getCurrentElement()));
			parent.setCurrentState(States.VARIABLE_DECLARATION.getState());
			parent.nextToken();
			
		} else
		{
			parent.getErrorHandler().printError("Unknown Statement.", parent.getNextToken(), ExitCodes.SYNTACTIC_ANALIZER_ERROR);
		}

	}

}
