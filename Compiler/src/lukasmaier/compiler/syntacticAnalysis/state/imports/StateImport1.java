package lukasmaier.compiler.syntacticAnalysis.state.imports;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.state.State;
import lukasmaier.compiler.syntacticAnalysis.state.States;

/**
 * State class which is used by the syntactical
 * analyzer internally to analyze the source code in form of
 * tokens and convert it to an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class StateImport1 extends State
{

	/**
	 * Creates a new state object
	 * @param parent
	 */
	public StateImport1(AbstractSyntaxTree parent)
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * on the provided token
	 * @param t token that the state is going to analyze
	 */
	@Override
	public void operate(Token t)
	{
		if (t.getType() == TokenType.QUOTATION_MARK)
		{
			parent.setCurrentState(States.IMPORT2.getState());
			parent.clearTokenBuffer();
			parent.addTokenToBuffer(t);
			parent.nextToken();
		} else
		{
			parent.getErrorHandler().printError("Invalid Expression in import statement", t, 
					ExitCodes.SYNTACTIC_ANALIZER_ERROR);
		}
		
	}

}
