package lukasmaier.compiler.syntacticAnalysis.state.imports;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElement;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElementType;
import lukasmaier.compiler.syntacticAnalysis.state.State;
import lukasmaier.compiler.syntacticAnalysis.state.States;

/**
 * State class which is used by the syntactical
 * analyzer internally to analyze the source code in form of
 * tokens and convert it to an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class StateImport2 extends State
{

	/**
	 * Creates a new state object
	 * @param parent
	 */
	public StateImport2(AbstractSyntaxTree parent)
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * on the provided token
	 * @param t token that the state is going to analyze
	 */
	@Override
	public void operate(Token t)
	{
		if (t.getType() == TokenType.QUOTATION_MARK && parent.getNextToken().getType() == TokenType.NEW_LINE)
		{
			parent.addTokenToBuffer(t);
			parent.addElementToAbstractSyntaxTree(new AbstractSyntaxTreeElement(AbstractSyntaxTreeElementType.IMPORT,
					parent.getTokenBufferContent(), parent.getCurrentElement()));
			parent.clearTokenBuffer();
			parent.setCurrentState(States.START.getState());
			parent.nextToken();
		} else if (t.getType() == TokenType.NEW_LINE)
		{
			parent.getErrorHandler().printError("Line break in import statement.", t,
					ExitCodes.SYNTACTIC_ANALIZER_ERROR);
		} else
		{
			parent.addTokenToBuffer(t);
			parent.nextToken();
		}
	}

}
