package lukasmaier.compiler.syntacticAnalysis.state;

import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;

/**
 * Super class for all state classes used by the syntactical
 * analyzer internally to analyze the source code in form of
 * tokens and convert it to an abstract syntax tree.
 * @author lukasmaier
 *
 */
public abstract class State
{
	protected AbstractSyntaxTree parent;
	
	/**
	 * Creates a new state object
	 * @param parent
	 */
	public State(AbstractSyntaxTree parent)
	{
		this.parent = parent;
	}
	
	/**
	 * Performs the action of the state depending
	 * on the provided token
	 * @param t token that the state is going to analyze
	 */
	public abstract void operate(Token t);
}
