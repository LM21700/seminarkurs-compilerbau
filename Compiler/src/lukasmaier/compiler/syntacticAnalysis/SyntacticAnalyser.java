package lukasmaier.compiler.syntacticAnalysis;

import lukasmaier.compiler.errorHandling.ErrorHandler;
import lukasmaier.compiler.lexer.TokenStorage;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.unneccessaryTokenRemoval.UnnecessaryTokenRemover;

/**
 * Class that analyzes the source code in form of tokens 
 * syntactically and creates an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class SyntacticAnalyser
{

	private TokenStorage oldTokens;
	private TokenStorage newTokens;
	private ErrorHandler errorHandler;
	
	/**
	 * Creates a new syntactic analyzer object.
	 * @param tokens token storage which should be analyzed
	 * @param errorHandler error handler that is responsible in the current context
	 */
	public SyntacticAnalyser(TokenStorage tokens, ErrorHandler errorHandler)
	{
		this.oldTokens = tokens;
		this.errorHandler = errorHandler;
	}
	
	/**
	 * 
	 * @return an abstract syntax tree
	 */
	public AbstractSyntaxTree createAst()
	{
		newTokens = new UnnecessaryTokenRemover(oldTokens).removeUnnecessaryTokens();
		
		return new AbstractSyntaxTree(newTokens, errorHandler);
	}
	
	

}
