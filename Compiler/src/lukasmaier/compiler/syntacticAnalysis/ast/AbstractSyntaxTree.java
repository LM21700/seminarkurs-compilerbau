package lukasmaier.compiler.syntacticAnalysis.ast;

import java.util.ArrayList;
import lukasmaier.compiler.errorHandling.ErrorHandler;
import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenStorage;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.syntacticAnalysis.ast.exception.AbstractSyntaxTreeNotYetCreatedException;
import lukasmaier.compiler.syntacticAnalysis.ast.exception.AlreadyAtMainElementException;
import lukasmaier.compiler.syntacticAnalysis.ast.exception.TreeDoesNotContainAnElementOfSpecifiedTypeAboveCurrentElementException;
import lukasmaier.compiler.syntacticAnalysis.state.State;
import lukasmaier.compiler.syntacticAnalysis.state.States;

public class AbstractSyntaxTree
{
	private TokenStorage tokens;
	private AbstractSyntaxTreeElement mainElement;
	private AbstractSyntaxTreeElement currentElement;
	private int index;
	private State currentState;
	private ArrayList<Token> tokenBuffer;
	private ErrorHandler errorHandler;
	private static AbstractSyntaxTree instance;

	/**
	 * Creates a new abstracts syntax tree out of the provided token storage.
	 * While creation it analyzes the source code syntactically.
	 * @param tokens token storage which should be transformed to an ast
	 * @param errorHandler the error handler which is responsible in the current
	 * context
	 */
	public AbstractSyntaxTree(TokenStorage tokens, ErrorHandler errorHandler)
	{
		this.tokens = tokens;
		this.errorHandler = errorHandler;
		tokenBuffer = new ArrayList<>();
		AbstractSyntaxTree.instance = this;
		currentState = States.START.getState();
		create();
	}
	
	/**
	 * Returns the instance of the abstract syntax tree if it exists.
	 * If no instance exists, it will thrown an exception
	 * @return instance of the abstract syntax tree
	 */
	public static AbstractSyntaxTree getInstance()
	{
		if (instance == null)
		{
			throw new AbstractSyntaxTreeNotYetCreatedException();
		} else
		{
			return instance;
		}
	}
	
	/**
	 * Increments the index of the token which will be analyzed from the
	 * token storage.
	 */
	public void nextToken()
	{
		this.index++;
	}
	
	/**
	 * Sets the current state of the internal analyzing mechanism to
	 * the provided state.
	 * @param s new current state
	 */
	public void setCurrentState(State s)
	{
		this.currentState = s;
	}
	
	/**
	 * Checks if the current token is at the beginning of the line
	 * @return whether the current token is at the beginning of the line or not
	 */
	public boolean isBeginOfLine()
	{
		return (index == 0 || tokens.getTokenAt(index - 1).getType() == TokenType.NEW_LINE);
	}
	
	/**
	 * adds an new element to the abstract syntax tree as following element of the current
	 * ast element and sets the new element as current element.
	 * @param e new abstract syntax tree element
	 */
	public void addElementToAbstractSyntaxTree(AbstractSyntaxTreeElement e)
	{
		currentElement.addFollowingElement(e);
		currentElement = e;
	}
	
	/**
	 * Returns the abstract syntax tree element of the ast that is active at the moment
	 * @return current ast element
	 */
	public AbstractSyntaxTreeElement getCurrentElement()
	{
		return currentElement;
	}
	
	/**
	 * Checks whether the current ast element has a parent of type BLOCK or not.
	 * @return if the current elment is in an function block
	 */
	public boolean isInFunctionBlock()
	{
		AbstractSyntaxTreeElement e = currentElement;
		while (currentElement.getType() != AbstractSyntaxTreeElementType.MAIN_ELEMENT)
		{
			if (currentElement.getType() == AbstractSyntaxTreeElementType.FUNCTION)
			{
				return true;
			}
			e = e.getParent();
		}
		return false;
	}
	
	/**
	 * Sets the current Element of the ast to the parent of the current element.
	 */
	public void goOneElementBack()
	{
		 
		if (currentElement instanceof AbstractSyntaxTreeMainElement)
		{
			throw new AlreadyAtMainElementException("Can't go any further. Already at main Element");
		}
		currentElement = currentElement.getParent();
	}
	
	/**
	 * Searches for an parent element of a specific type of the current ast element and
	 * sets it as the current element.
	 * @param t type of the parent element
	 */
	public void goBackToLastElementOfType(AbstractSyntaxTreeElementType t)
	{
		AbstractSyntaxTreeElement e = currentElement;
		do
		{
			if (currentElement instanceof AbstractSyntaxTreeMainElement)
			{
				throw new TreeDoesNotContainAnElementOfSpecifiedTypeAboveCurrentElementException();
			}
			
			e = e.getParent();
			
		} while (e.getType() != t);
		
		currentElement = e;
	}
	
	/**
	 * Adds an token to the token buffer.
	 * @param t token which should be added
	 */
	public void addTokenToBuffer (Token t)
	{
		tokenBuffer.add(t);
	}
	
	/**
	 * Returns the token buffer.
	 * @return token buffer
	 */
	public ArrayList<Token> getTokenBuffer()
	{
		return tokenBuffer;
	}
	
	/**
	 * Concatenates the content from all tokens in the 
	 * token buffer and returns it as String.
	 * @return content of the token buffer
	 */
	public String getTokenBufferContent()
	{
		StringBuilder s = new StringBuilder();
		for (Token t : tokenBuffer)
		{
			s.append(t.getContent());
		}
		
		return s.toString();
	}
	
	/**
	 * Removes all elements from the token buffer.
	 */
	public void clearTokenBuffer()
	{
		tokenBuffer = new ArrayList<>();
	}
	
	/**
	 * Returns the error handler object that is responsible for
	 * error handling in the current context. 
	 * @return error handler
	 */
	public ErrorHandler getErrorHandler()
	{
		return errorHandler;
	}
	
	/**
	 * Returns the token at the provided index of the token storage
	 * @param index index in token storage
	 * @return token at the index of the token storage
	 */
	public Token getTokenAt(int index)
	{
		// TODO check if index is in range
		return tokens.getTokenAt(index);
	}
	
	/**
	 * Returns the previous token without decrementing the
	 * index
	 * @return token at index - 1
	 */
	public Token getPreviousToken()
	{
		return tokens.getTokenAt(this.index - 1);
	}
	
	/**
	 * Returns the next token of the token storage without 
	 * incrementing the index.
	 * @return token at index + 1
	 */
	public Token getNextToken()
	{
		return tokens.getTokenAt(this.index  + 1);
	}
	
	/**
	 * Sets the current element to the main element of the
	 * abstract syntax tree. 
	 */
	public void toMainElement()
	{
		currentElement = mainElement;
	}

	private void create()
	{
		this.index = 0;
		mainElement = new AbstractSyntaxTreeMainElement();
		currentElement = mainElement;
		while (index < tokens.size())
		{
			currentState.operate(tokens.getTokenAt(index));
		}
	}
}
