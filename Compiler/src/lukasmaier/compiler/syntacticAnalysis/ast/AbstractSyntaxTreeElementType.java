package lukasmaier.compiler.syntacticAnalysis.ast;

/**
 * Contains the different types that a abstract syntax tree element could have
 * @author lukasmaier
 *
 */
public enum AbstractSyntaxTreeElementType
{
	MAIN_ELEMENT,
	IMPORT,
	VARIABLE,
	FUNCTION,
	FUNCTION_NAME,
	BLOCK,
	ARGUEMENTS,
	RETURN_TYPE,
	IF_CONDITION,
	WHILE_LOOP,
	FOR_LOOP,
	CONDITION,
	ITERATOR,
	ITERATOR_CHANGE,
	ASSIGNMENT,
	OPERATOR,
	ARIMETHIC_OPERATOR,
	VARIBALE_TYPE,
	VARIABLE_DECLARATION,
	FUNCTION_CALL,
	AND,
	OR,
	INTEGER,
	FLOAT;

}
