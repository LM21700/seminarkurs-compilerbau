package lukasmaier.compiler.syntacticAnalysis.ast;

import java.util.ArrayList;

import lukasmaier.compiler.syntacticAnalysis.ast.exception.ElementDoesNotContainAFollowingElementOfSpecifiedTypeException;


public class AbstractSyntaxTreeElement
{
	private AbstractSyntaxTreeElementType type;
	private String value;
	private AbstractSyntaxTreeElement parent;
	private Integer indentation; // for null values necessary
	private ArrayList<AbstractSyntaxTreeElement> followingElements;
	
	/**
	 * Creates a new abstract syntax tree element. <br>
	 * Indentation, value and followingElements are optional, the rest should not
	 * be null.
	 * @param type ast element type of the element
	 * @param value value of the element (from token value)
	 * @param parent parent object of this element
	 * @param followingElements list of elements that follow on the current element
	 * @param indentation indentation of the current element
	 */
	public AbstractSyntaxTreeElement(AbstractSyntaxTreeElementType type, String value, AbstractSyntaxTreeElement parent,
			ArrayList<AbstractSyntaxTreeElement> followingElements, Integer indentation)
	{
		this.type = type;
		this.value = value;
		this.parent = parent;
		this.followingElements = (followingElements != null) ? followingElements : new ArrayList<>();
		this.indentation = indentation;
	}
	
	/**
	 * Creates a new abstract syntax tree element.
	 * @param type ast element type of the element
	 * @param value value of the element (from token value)
	 * @param parent parent object of this element
	 * @param indentation indentation of the current element
	 */
	public AbstractSyntaxTreeElement(AbstractSyntaxTreeElementType type, String value, AbstractSyntaxTreeElement parent,
			Integer indentation)
	{
		this(type, value, parent, null, indentation);
	}
	
	/**
	 * Creates a new abstract syntax tree element.
	 * @param type ast element type of the element
	 * @param value value of the element (from token value)
	 * @param parent parent object of this element
	 */
	public AbstractSyntaxTreeElement(AbstractSyntaxTreeElementType type, String value, AbstractSyntaxTreeElement parent)
	{
		this(type, value, parent, null, null);
	}

	/**
	 * Returns the type of the abstract syntax tree element.
	 * @return type of the ast element
	 */
	public AbstractSyntaxTreeElementType getType() 
	{
		return type;
	}

	/**
	 * Returns the value of the abstract syntax tree element
	 * @return value of the element
	 */
	public String getValue()
	{
		return value;
	}

	/**
	 * Returns all of the following elements of this element as ArrayList
	 * of AbstractSyntaxTreeElements
	 * @return following ast elements
	 */
	public ArrayList<AbstractSyntaxTreeElement> getFollowingElements() 
	{
		
		return followingElements;
	}
	
	/**
	 * Adds an ast element to this element as following element.
	 * @param element element which should be added
	 */
	public void addFollowingElement(AbstractSyntaxTreeElement element)
	{
		followingElements.add(element);
	}
	
	/**
	 * Returns the parent element of this element.
	 * @return parent element
	 */
	public AbstractSyntaxTreeElement getParent()
	{
		return parent;
	}
	
	/**
	 * Sets an abstract syntax tree element as parent of this element.
	 * @param parent parent element
	 */
	public void setParent(AbstractSyntaxTreeElement parent)
	{
		this.parent = parent;
	}
	
	/**
	 * Returns the current indentation of the element. <br>
	 * Can be either null or >= 0. Is only != null if element is
	 * of type BLOCK
	 * @return indentation of the element
	 */
	public Integer getIdentation()
	{
		return indentation;
	}
	
	/**
	 * Sets the indentation of the element.
	 * @param indentation indentation of the element
	 */
	public void setIndentation(Integer indentation)
	{
		this.indentation = indentation;
	}
	
	/**
	 * Returns whether this element has a following element of the provided type or not
	 * @param type type for which should be searched
	 * @return if element has a following element of the provided type
	 */
	public boolean hasFollowingElementOfType(AbstractSyntaxTreeElementType type)
	{
		for (int i = 0; i < followingElements.size(); i++)
		{
			if (followingElements.get(i).getType() == type)
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Returns the first element of a provided type out of the list of following
	 * elements of this element.
	 * @param type type of following element
	 * @return following element of specified type
	 */
	public AbstractSyntaxTreeElement getFollowingElementOfType(AbstractSyntaxTreeElementType type)
	{
		for (int i = 0; i < followingElements.size(); i++)
		{
			if (followingElements.get(i).getType() == type)
			{
				return followingElements.get(i);
			}
		}
		throw new ElementDoesNotContainAFollowingElementOfSpecifiedTypeException();
	}
		
}
