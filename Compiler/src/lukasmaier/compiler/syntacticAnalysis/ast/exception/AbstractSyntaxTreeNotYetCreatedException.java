package lukasmaier.compiler.syntacticAnalysis.ast.exception;

/**
 * Exception that gets thrown if it is asked for an instance of AbstractSyntaxTree,
 * but no object is created until now.
 * @author lukasmaier
 *
 */
public class AbstractSyntaxTreeNotYetCreatedException extends RuntimeException
{

	private static final long serialVersionUID = 1369203864142254611L;

	public AbstractSyntaxTreeNotYetCreatedException()
	{
	}

	public AbstractSyntaxTreeNotYetCreatedException(String arg0)
	{
		super(arg0);
	}

	public AbstractSyntaxTreeNotYetCreatedException(Throwable arg0)
	{
		super(arg0);
	}

	public AbstractSyntaxTreeNotYetCreatedException(String arg0, Throwable arg1)
	{
		super(arg0, arg1);
	}

	public AbstractSyntaxTreeNotYetCreatedException(String arg0, Throwable arg1, boolean arg2, boolean arg3)
	{
		super(arg0, arg1, arg2, arg3);
	}

}
