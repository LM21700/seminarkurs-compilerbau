package lukasmaier.compiler.syntacticAnalysis.ast.exception;

/**
 * Exception that gets thrown if asked for an element of a specific type above current
 * element, but there is no element of this type above the current element.
 * @author lukasmaier
 *
 */
public class TreeDoesNotContainAnElementOfSpecifiedTypeAboveCurrentElementException extends RuntimeException
{

	private static final long serialVersionUID = 8288454098127729130L;

	public TreeDoesNotContainAnElementOfSpecifiedTypeAboveCurrentElementException()
	{
	}

	public TreeDoesNotContainAnElementOfSpecifiedTypeAboveCurrentElementException(String message)
	{
		super(message);
	}

	public TreeDoesNotContainAnElementOfSpecifiedTypeAboveCurrentElementException(Throwable cause)
	{
		super(cause);
	}

	public TreeDoesNotContainAnElementOfSpecifiedTypeAboveCurrentElementException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public TreeDoesNotContainAnElementOfSpecifiedTypeAboveCurrentElementException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
