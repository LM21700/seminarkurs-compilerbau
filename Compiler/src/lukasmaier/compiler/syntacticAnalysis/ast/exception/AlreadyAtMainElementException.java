package lukasmaier.compiler.syntacticAnalysis.ast.exception;

/**
 * Gets thrown if asked for element above the current element, but there is no element above.
 * @author lukasmaier
 *
 */
public class AlreadyAtMainElementException extends RuntimeException
{

	private static final long serialVersionUID = -6761806624438870460L;

	public AlreadyAtMainElementException()
	{
	}

	public AlreadyAtMainElementException(String arg0)
	{
		super(arg0);
	}

	public AlreadyAtMainElementException(Throwable arg0)
	{
		super(arg0);
	}

	public AlreadyAtMainElementException(String arg0, Throwable arg1)
	{
		super(arg0, arg1);
	}

	public AlreadyAtMainElementException(String arg0, Throwable arg1, boolean arg2, boolean arg3)
	{
		super(arg0, arg1, arg2, arg3);
	}

}
