package lukasmaier.compiler.syntacticAnalysis.ast.exception;

/**
 * Exception that gets thrown if it is asked for a following element of an AbstratSyntaxTreeElement
 * of a specific type, but it has not an following element of this type.
 * @author lukasmaier
 *
 */
public class ElementDoesNotContainAFollowingElementOfSpecifiedTypeException extends RuntimeException 
{

	private static final long serialVersionUID = -3833546911657173411L;

	public ElementDoesNotContainAFollowingElementOfSpecifiedTypeException() 
	{
	}

	public ElementDoesNotContainAFollowingElementOfSpecifiedTypeException(String arg0)
	{
		super(arg0);
	}

	public ElementDoesNotContainAFollowingElementOfSpecifiedTypeException(Throwable arg0)
	{
		super(arg0);
	}

	public ElementDoesNotContainAFollowingElementOfSpecifiedTypeException(String arg0, Throwable arg1) 
	{
		super(arg0, arg1);

	}

	public ElementDoesNotContainAFollowingElementOfSpecifiedTypeException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) 
	{
		super(arg0, arg1, arg2, arg3);

	}

}
