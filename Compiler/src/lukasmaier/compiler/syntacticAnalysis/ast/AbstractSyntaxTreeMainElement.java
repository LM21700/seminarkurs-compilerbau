package lukasmaier.compiler.syntacticAnalysis.ast;

/**
 * Main element of the abstract syntax tree.
 * @author lukasmaier
 *
 */
public class AbstractSyntaxTreeMainElement extends AbstractSyntaxTreeElement
{
	/**
	 * Creates a new abstract syntax tree main element
	 */
	public AbstractSyntaxTreeMainElement()
	{
		super(AbstractSyntaxTreeElementType.MAIN_ELEMENT, null, null, new Integer(0));
	}
}
