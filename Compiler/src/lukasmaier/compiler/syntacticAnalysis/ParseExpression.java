package lukasmaier.compiler.syntacticAnalysis;

import lukasmaier.compiler.errorHandling.ErrorHandler;
import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenStorage;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElement;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElementType;

/**
 * Used to translate complex expressions from tokens to ast elements.
 * @author lukasmaier
 *
 */
public abstract class ParseExpression
{

	/**
	 * Translates an complex expression from tokens to an abstract syntax tree
	 * element
	 * @param tokens expression
	 * @param errorHandler error handler
	 * @return translated tokens
	 */
	public static AbstractSyntaxTreeElement parseExpression(TokenStorage tokens, ErrorHandler errorHandler)
	{
		AbstractSyntaxTreeElement currentElement = null;
		

		/*// split at parens
		int parenLevel = 0;
		for (int i = 0; i < tokens.size(); i++)
		{
			Token currentToken = tokens.getTokenAt(i);
			if (currentToken.getType() == TokenType.OPEN_PARENTHESIS)
			{
				parenLevel++;
			} else if (currentToken.getType() == TokenType.CLOSING_PARENTHESIS)
			{
				parenLevel--;
			} else if (currentToken.getType() == TokenType.AND && parenLevel == 0)
			{
				AbstractSyntaxTreeElement newElement = new AbstractSyntaxTreeElement(AbstractSyntaxTreeElementType.AND, currentToken.getContent(), null);
				if (currentElement != null)
				{
					currentElement.addFollowingElement(newElement);
				}
				currentElement = newElement;
				currentElement.addFollowingElement(parseExpression(null)); // TODO new TokenStorage
			}
			
		}*/
		
		// plus and minus
		for (int i = 0; i < tokens.size(); i++)
		{
			Token currentToken = tokens.getTokenAt(i);
			if (currentToken.getType() == TokenType.PLUS || currentToken.getType() == TokenType.MINUS)
			{
				AbstractSyntaxTreeElement newElement = new AbstractSyntaxTreeElement(
						AbstractSyntaxTreeElementType.ARIMETHIC_OPERATOR, currentToken.getContent(), null);
			
				// TODO check if currentElement is not empty every this this gets hit
				if (currentElement == null)
				{
					currentElement = newElement;
				} else
				{
					newElement.setParent(currentElement);
					currentElement.addFollowingElement(newElement);
				}
				
				newElement.addFollowingElement(parseExpression(new TokenStorage(tokens, 0, i), errorHandler));
				newElement.addFollowingElement(parseExpression(new TokenStorage(tokens, i + 1, tokens.size()), errorHandler));
				return currentElement;
			}
		}
		
		// numbers		
		if (tokens.size() > 1)
		{
			errorHandler.printError("Unknown expression", tokens.getTokenAt(0), ExitCodes.SYNTACTIC_ANALIZER_ERROR);
		} else if (tokens.size() == 1)
		{
			Token currentToken = tokens.getTokenAt(0);
			if (currentToken.getType() == TokenType.INTEGER)
			{
				AbstractSyntaxTreeElement newElement = new AbstractSyntaxTreeElement(AbstractSyntaxTreeElementType.INTEGER,
						currentToken.getContent(), null);
				
				// TODO check if currentElement is not empty every this this gets hit
				currentElement = newElement;
			} else if (currentToken.getType() == TokenType.FLOAT)
			{
				AbstractSyntaxTreeElement newElement = new AbstractSyntaxTreeElement(AbstractSyntaxTreeElementType.FLOAT,
						currentToken.getContent(), null);
				// TODO check if currentElement is not empty every this this gets hit
				currentElement = newElement;
			}
		}
		
		return currentElement;
	}

}
