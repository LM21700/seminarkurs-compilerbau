package lukasmaier.compiler;

import java.io.FileNotFoundException;
import java.io.IOException;

import lukasmaier.compiler.codeGeneration.CodeGenerator;
import lukasmaier.compiler.errorHandling.ErrorHandler;
import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Lexer;
import lukasmaier.compiler.lexer.TokenStorage;
import lukasmaier.compiler.semanticAnalysis.SemanticAnalyser;
import lukasmaier.compiler.syntacticAnalysis.SyntacticAnalyser;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.util.IOUtil;

// LONGTERM check if only Spaces or only tabs are used
// LONGTERM compile outputfile directly with gcc/mingw
/**
 * Main class of the compiler
 * @author lukasmaier
 *
 */
public class Compiler 
{
	private String pathToSourceCode;
	private String sourceCode;
	private ErrorHandler errorHandler;
	
	public static void main (String[] args)
	{
		Compiler c = new Compiler(args);
		c.run();		
	}
	
	/**
	 * Creates a new Compiler object.
	 * The compiler can be executed by the {@code run()} function
	 * of the object 
	 * 
	 * @param args arguements with the program is called, like the
	 * path to the sourcecode file
	 */
	public Compiler(String[] args)
	{
		parseArgs(args);
	}
	
	private void run()
	{
		// create ErrorHandler
		errorHandler = new ErrorHandler();
		
		// read source code
		readSourceCode();
		
		// lexicalic analysis
		Lexer lexer = new Lexer(sourceCode, errorHandler);
		TokenStorage t = lexer.tokenize();
		errorHandler.setTokenStorage(t);
		
		// syntactic analysis
		SyntacticAnalyser syntacticAnalyser = new SyntacticAnalyser(t, errorHandler);
		AbstractSyntaxTree ast = syntacticAnalyser.createAst();
		
		// semantic analysis
		SemanticAnalyser semanticAnalyser = new SemanticAnalyser(ast, errorHandler);
		semanticAnalyser.analyse();
		
		// code generation
		CodeGenerator codeGenerator = new CodeGenerator(ast, errorHandler);
		String targetCode = codeGenerator.generate();
		
		// write target code
		writeTargetCode(targetCode);
		
	}
	
	private void parseArgs(String[] args)
	{
		if (args.length == 0)
		{
			usage();
		}
		pathToSourceCode = args[args.length-1];
	}
	
	private void readSourceCode()
	{
		try
		{
			sourceCode = IOUtil.readFile(pathToSourceCode);
		}catch (FileNotFoundException e)
		{
			errorHandler.printError("File not found.\nCheck the file path and try again.", ExitCodes.ILLEGAL_FILE_PATH);
			
		}catch (IOException e)
		{
			errorHandler.printError("IO Error", ExitCodes.IO_Error);
		}
	}
	
	private void writeTargetCode(String targetCode)
	{
		try
		{
			IOUtil.writeFile(getFilename() + ".c", targetCode);
		} catch (IOException e)
		{
			errorHandler.printError("IO Error", ExitCodes.IO_Error);
		}
	}
	
	// IMPLEMENT getFilename() in class compiler
	private String getFilename()
	{
		return "out";
	}
	
	private void usage()
	{
		errorHandler.exitWithComment("Usage: <Programmname> <pathToSource>", ExitCodes.ILLEGAL_ARGUEMENTS);
	}
}
