package lukasmaier.compiler.lexer;

import java.util.ArrayList;

/**
 * Data structure for storing and managing multiple
 * tokens.
 * @author lukasmaier
 *
 */
public class TokenStorage 
{
	ArrayList<Token> tokens;
	
	/**
	 * Creates an empty TokenStorage
	 */
	public TokenStorage()
	{
		tokens = new ArrayList<>();
	}
		
	/**
	 * Creates a TokenStorage out of a list of tokens.
	 * @param tokens list out of which the TokenStorage should
	 * be created
	 */
	public TokenStorage(ArrayList<Token> tokens)
	{
		this.tokens = tokens;
	}
	
	/**
	 * creates a TokenStorage out of another TokenStorage and an
	 * beginning end ending Index. It copies all of the Tokens in
	 * this range from the provided TokenStorage to the new TokenStorage <br>
	 * inclusive the start element, exclusive the end element
	 * @param t TokenStorage from which a part of the Tokens should be
	 * copied into the new one
	 * @param start beginning index
	 * @param end end index
	 */
	public TokenStorage(TokenStorage t, int start, int end)
	{
		if (start < 0 || end > t.size())
		{
			throw new IndexOutOfBoundsException();
		}
		
		tokens = new ArrayList<>();
		for (int i = start; i < end; i++)
		{
			tokens.add(t.getTokenAt(i));
		}
	}

	/**
	 * adds a token to the TokenStorage
	 * @param t Token to add
	 */
	public void addToken(Token t)
	{
		tokens.add(t);
	}
	
	/**
	 * Returns the amount of tokens in the TokenStorage
	 * @return size of TokenStorage
	 */
	public int size()
	{
		return tokens.size();
	}
	
	/**
	 * Returns the token at a specific index.
	 * @param index
	 * @return Token at specified index.
	 */
	public Token getTokenAt(int index)
	{
		return tokens.get(index);
	}
	
	/**
	 * Removes provided token from TokenStorage.
	 * @param t Token that should be removed.
	 */
	public void removeToken(Token t)
	{
		tokens.remove(t);
	}
	
	@Override
	public TokenStorage clone()
	{
		TokenStorage ts = new TokenStorage();
		for (Token t : tokens)
		{
			ts.addToken(t);
		}
		return ts;
	}
	
	@Override
	public String toString()
	{
		StringBuilder s = new StringBuilder();
		
		for (Token t : tokens)
		{
			s.append(t.toString());
			s.append('\n');
		}
		
		return s.toString();
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof TokenStorage)
		{
			TokenStorage newStorage = (TokenStorage) o;
			if (newStorage.size() == this.size())
			{
				for (int i = 0; i < this.size(); i++)
				{
					if (!this.getTokenAt(i).equals(newStorage.getTokenAt(i)))
					{
						return false;
					}
				}
				return true;
			}
		} 
		return false;
	}
	
}
