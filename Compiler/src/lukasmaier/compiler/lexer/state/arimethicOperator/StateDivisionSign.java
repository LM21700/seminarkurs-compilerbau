package lukasmaier.compiler.lexer.state.arimethicOperator;

import lukasmaier.compiler.lexer.Lexer;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.lexer.state.State;
import lukasmaier.compiler.lexer.state.States;

/**
 * State class which is used in the State-Pattern in
 * the Lexer.
 * @author lukasmaier
 *
 */
public class StateDivisionSign extends State
{

	/**
	 * creates a new State object
	 * @param parent
	 */
	public StateDivisionSign(Lexer parent)
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * of the provided char 
	 * @param c char that the state is going to analyze
	 */
	@Override
	public void operate(char c) 
	{
		if (c == '=')
		{
			parent.setCurrentState(States.DIVISION_ASSIGNMENT.getState());
			parent.nextChar();
		} else
		{
			parent.addToken(TokenType.DIVISION_SIGN);
			parent.setCurrentState(States.START.getState());
		}

	}

}
