package lukasmaier.compiler.lexer.state.punctuation;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Lexer;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.lexer.state.State;
import lukasmaier.compiler.lexer.state.States;

/**
 * State class which is used in the State-Pattern in
 * the Lexer.
 * @author lukasmaier
 *
 */
public class StateString1 extends State
{
	// LONGTERM implement escape sequenzes in Strings
	/**
	 * creates a new State object
	 * @param parent
	 */
	public StateString1(Lexer parent)
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * of the provided char 
	 * @param c char that the state is going to analyze
	 */
	@Override
	public void operate(char c)
	{
		if (c == '"')
		{
			parent.addToken(TokenType.STRING);
			parent.setCurrentState(States.START.getState());
		} else if (c == '\n')
		{
			parent.getErrorHandler().printError("Unexpected symbol.", parent.getCurrentLine(), ExitCodes.LEXER_ERROR);
		}
		
		parent.nextChar();
	}

}
