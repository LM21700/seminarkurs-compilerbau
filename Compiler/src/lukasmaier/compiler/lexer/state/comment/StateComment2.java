package lukasmaier.compiler.lexer.state.comment;

import lukasmaier.compiler.lexer.Lexer;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.lexer.state.State;
import lukasmaier.compiler.lexer.state.States;

/**
 * State class which is used in the State-Pattern in
 * the Lexer.
 * @author lukasmaier
 *
 */
public class StateComment2 extends State 
{

	/**
	 * creates a new State object
	 * @param parent
	 */
	public StateComment2(Lexer parent) 
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * of the provided char 
	 * @param c char that the state is going to analyze
	 */
	@Override
	public void operate(char c) 
	{
		if (c == '\n')
		{
			parent.setCurrentState(States.START.getState());
			parent.addToken(TokenType.COMMENT);
		} else
		{
			parent.nextChar();
		}
	
		
	}

}
