package lukasmaier.compiler.lexer.state;

import java.lang.reflect.InvocationTargetException;

import lukasmaier.compiler.lexer.Lexer;
import lukasmaier.compiler.lexer.state.comment.*;
import lukasmaier.compiler.lexer.state.number.*;
import lukasmaier.compiler.lexer.state.whitespace.*;
import lukasmaier.compiler.lexer.state.arimethicOperator.*;
import lukasmaier.compiler.lexer.state.booleanOperator.*;
import lukasmaier.compiler.lexer.state.identifier.*;
import lukasmaier.compiler.lexer.state.punctuation.*;
import lukasmaier.compiler.lexer.state.parenthesis.*;
import lukasmaier.compiler.lexer.state.assignment.*;

/**
 * Stores all possible states the Lexer could be in.
 * @author lukasmaier
 *
 */
public enum States 
{	
	// XXX Destroy objects after lexer is finished?
	START(StateStart.class),
	COMMENT1(StateComment1.class),
	COMMENT2(StateComment2.class),
	COMMENT3(StateComment3.class),
	COMMENT4(StateComment4.class),
	INTEGER(StateInteger.class),
	FLOAT(StateFloat.class),
	NEW_LINE(StateNewLine.class),
	TABULATOR(StateTabulator.class),
	DIVISION_SIGN(StateDivisionSign.class),
	PLUS(StatePlus.class),
	MINUS(StateMinus.class),
	MODULO(StateModulo.class),
	MULTIPLICATION_SIGN(StateMultiplicationSign.class),
	AMPERSAND(StateAmpersand.class),
	DOUBLE_AMPERSAND(StateDoubleAmpersand.class),
	OR(StateOr.class),
	DoubleOr(StateDoubleOr.class),
	IDENTIFIER(StateIdentifier.class),
	COLON(StateColon.class),
	COMMA(StateComma.class),
	CLOSING_BRACKET(StateClosingBracket.class),
	CLOSING_CURLY_BRACKET(StateClosingCurlyBracket.class),
	CLOSING_PARENTHESIS(StateClosingParenthesis.class),
	OPEN_BRACKET(StateOpenBracket.class),
	OPEN_CURLY_BRACKET(StateOpenCurlyBracket.class),
	OPEN_PARENTHESIS(StateOpenParenthesis.class),
	BLANK(StateBlank.class),
	EQUALITY(StateEquality.class),
	ASSIGNMENT(StateAssignment.class),
	PLUS_ASSIGNMENT(StatePlusAssignment.class),
	MINUS_ASSIGNMENT(StateMinusAssignment.class),
	DIVISION_ASSIGNMENT(StateDivisionAssignment.class),
	MULTIPLICATION_ASSIGNMENT(StateMultiplicationAssignment.class),
	SMALLER(StateSmaller.class),
	GREATER(StateGreater.class),
	SMALLER_OR_EQUAL(StateSmallerOrEqual.class),
	GREATER_OR_EQUAL(StateGreaterOrEqual.class),
	POINT(StatePoint.class),
	STRING(StateString1.class),
	SINGLE_QUOTATION_MARK(StateSingleQuotationMark.class);
	
	private State state;
	private Class<? extends State> s;
	
	private States(Class<? extends State> s)
	{
		this.s = s;
	}
	
	/**
	 * Returns the state which the enum field represents.
	 * @return State
	 */
	public State getState()
	{
		if (state == null)
		{
			try 
			{
				state = (State) s.getConstructor(Lexer.class).newInstance(Lexer.getInstance());
			} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) 
			{
				e.printStackTrace();
			}
		}
		return state;
	}
}
