package lukasmaier.compiler.lexer.state.identifier;

import java.util.ArrayList;

/**
 * Class that manages the identification of keywords.
 * @author lukasmaier
 *
 */
public abstract class Keyword
{
	private static ArrayList<String> keywords;
	
	static
	{
		keywords = new ArrayList<>();
		
		keywords.add("function");
		//keywords.add("string");
		//keywords.add("int");
		keywords.add("float");
		keywords.add("private");
		keywords.add("public");
		keywords.add("if");
		keywords.add("else");
		keywords.add("while");
		keywords.add("for");
		keywords.add("import");
	}
	
	/**
	 * Checks whether the provided string is a keyword or not. 
	 * @param word possible keyword
	 * @return if provided String is a keyword or not
	 */
	public static boolean isKeyword(String word)
	{
		for (String keyword : getKeywords())
		{
			if (keyword.equals(word))
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns all keywords in form of a ArrayList filled with strings.
	 * @return all keywords
	 */
	private static ArrayList<String> getKeywords()
	{
		return keywords;
	}

}
