package lukasmaier.compiler.lexer.state.identifier;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Lexer;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.lexer.state.State;
import lukasmaier.compiler.lexer.state.States;
import lukasmaier.util.CharUtil;

/**
 * State class which is used in the State-Pattern in
 * the Lexer.
 * @author lukasmaier
 *
 */
public class StateIdentifier extends State
{

	/**
	 * creates a new State object
	 * @param parent
	 */
	public StateIdentifier(Lexer parent)
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * of the provided char 
	 * @param c char that the state is going to analyze
	 */
	@Override
	public void operate(char c) 
	{
		if (CharUtil.isWhitespace(c) || CharUtil.isArimethicOperator(c) || CharUtil.isParenthesis(c) || c == '=' || c == '#' || c == ',')
		{
			parent.setCurrentState(States.START.getState());
			// check if it is keyword
			if (Keyword.isKeyword(parent.getCurrentToken()))
			{
				parent.addToken(TokenType.KEYWORD);
			} else
			{
				parent.addToken(TokenType.IDENTIFIER);
			}
		} else if (CharUtil.isLetter(c) || CharUtil.isDigit(c) || c == '_')
		{
			parent.nextChar();
		} else if (c == '.')
		{
			parent.setCurrentState(States.POINT.getState());
			parent.addToken(TokenType.IDENTIFIER);
			parent.nextChar();
		} else if (c == '"' || c == '\'')
		{
			parent.addToken(TokenType.IDENTIFIER);
			parent.setCurrentState(States.START.getState());
		} else
		{
			parent.getErrorHandler().printError("Unexpected Symbol.", parent.getCurrentLine(), ExitCodes.LEXER_ERROR);
		}

	}

}
