package lukasmaier.compiler.lexer.state;

import lukasmaier.compiler.lexer.Lexer;

/**
 * Parent class for all classes used by the 
 * State-Pattern which is used in the Lexer class.
 * @author lukasmaier
 *
 */
public abstract class State 
{
	protected Lexer parent;
	
	/**
	 * creates a new State object
	 * @param parent
	 */
	public State(Lexer parent)
	{
		this.parent = parent;
	}
	
	/**
	 * Performs the action of the state depending
	 * on the provided char 
	 * @param c char that the state is going to analyze
	 */
	public abstract void operate(char c);
}
