package lukasmaier.compiler.lexer.state;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Lexer;
import lukasmaier.util.CharUtil;

/**
 * State class which is used in the State-Pattern in
 * the Lexer.
 * @author lukasmaier
 *
 */
public class StateStart extends State
{

	/**
	 * creates a new State object
	 * @param parent
	 */
	public StateStart(Lexer parent) 
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * of the provided char 
	 * @param c char that the state is going to analyze
	 */
	@Override
	public void operate(char c) 
	{
		// comments
		if (c == '#')
		{
			parent.setCurrentState(States.COMMENT1.getState());
			
		} else if (c == '\n')
		{ 
			parent.setCurrentState(States.NEW_LINE.getState());
		} else if (c == '\t')
		{
			parent.setCurrentState(States.TABULATOR.getState());
		}else if (c == ' ')
		{
			parent.setCurrentState(States.BLANK.getState());
		} else if (CharUtil.isDigit(c))
		{
			parent.setCurrentState(States.INTEGER.getState());
		}else if (c == '.')
		{
			parent.setCurrentState(States.FLOAT.getState());
		}else if (c == ':')
		{
			parent.setCurrentState(States.COLON.getState());
		}else if (CharUtil.isLetter(c))
		{
			parent.setCurrentState(States.IDENTIFIER.getState());
		}else if (c == '(')
		{
			parent.setCurrentState(States.OPEN_PARENTHESIS.getState());
		}else if (c == ')')
		{
			parent.setCurrentState(States.CLOSING_PARENTHESIS.getState());
		}else if (c == '[')
		{
			parent.setCurrentState(States.OPEN_BRACKET.getState());
		}else if (c == ']')
		{
			parent.setCurrentState(States.CLOSING_BRACKET.getState());
		}else if (c == '+')
		{
			parent.setCurrentState(States.PLUS.getState());
		}else if (c == '-')
		{
			parent.setCurrentState(States.MINUS.getState());
		}else if (c == '*')
		{
			parent.setCurrentState(States.MULTIPLICATION_SIGN.getState());
		}else if (c == '/')
		{
			parent.setCurrentState(States.DIVISION_SIGN.getState());
		}else if (c == '%')
		{
			parent.setCurrentState(States.MODULO.getState());
		}else if (c == '&')
		{
			parent.setCurrentState(States.AMPERSAND.getState());
		}else if (c == '|')
		{
			parent.setCurrentState(States.OR.getState());
		}else if (c == '=')
		{
			parent.setCurrentState(States.ASSIGNMENT.getState());
		}else if (c == '<')
		{
			parent.setCurrentState(States.SMALLER.getState());
		}else if (c == '>')
		{
			parent.setCurrentState(States.GREATER.getState());
		}else if (c == '"')
		{
			parent.setCurrentState(States.STRING.getState());
		}else if (c == '\'')
		{
			parent.setCurrentState(States.SINGLE_QUOTATION_MARK.getState());
		} else if (c == ',')
		{
			parent.setCurrentState(States.COMMA.getState());
		} else
		{
			parent.getErrorHandler().printError("Unexpected Symbol", parent.getCurrentLine(), ExitCodes.LEXER_ERROR);
		}
		parent.nextChar();
		
	}
	
}
