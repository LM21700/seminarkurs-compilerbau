package lukasmaier.compiler.lexer.state.number;

import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.lexer.Lexer;
import lukasmaier.compiler.lexer.TokenType;
import lukasmaier.compiler.lexer.state.State;
import lukasmaier.compiler.lexer.state.States;
import lukasmaier.util.CharUtil;

/**
 * State class which is used in the State-Pattern in
 * the Lexer.
 * @author lukasmaier
 *
 */
public class StateInteger extends State
{
	
	/**
	 * creates a new State object
	 * @param parent
	 */
	public StateInteger(Lexer parent)
	{
		super(parent);
	}

	/**
	 * Performs the action of the state depending
	 * of the provided char 
	 * @param c char that the state is going to analyze
	 */
	@Override
	public void operate(char c) 
	{
		if (c == '.')
		{
			parent.setCurrentState(States.FLOAT.getState());
			parent.nextChar();
		} else if (CharUtil.isDigit(c))
		{
			parent.nextChar();
		} else if (CharUtil.isWhitespace(c) || CharUtil.isParenthesis(c) || CharUtil.isArimethicOperator(c) || c == '#' || c == '=')
		{
			parent.setCurrentState(States.START.getState());
			parent.addToken(TokenType.INTEGER);
		} else
		{
			parent.getErrorHandler().printError("Unexpected Symbol.", parent.getCurrentLine(), ExitCodes.LEXER_ERROR);
		}
	}

}
