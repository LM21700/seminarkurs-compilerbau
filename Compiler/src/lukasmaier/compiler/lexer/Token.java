package lukasmaier.compiler.lexer;

/**
 * Data structure that consists of a String as content
 * and a type. Used to store the tokenized source code
 * @author lukasmaier
 *
 */
public class Token 
{
	private final String CONTENT;
	private final TokenType TYPE;
	
	/**
	 * creates a new Token. <br>
	 * Content and type can not be changed later.
	 * @param content content of the Token
	 * @param type a element of TokenType
	 */
	public Token(String content, TokenType type)
	{
		this.CONTENT = content;
		this.TYPE = type;
	}
	
	/**
	 * Returns the content of the token.
	 * @return content of the token
	 */
	public String getContent()
	{
		return CONTENT;
	}
	
	/**
	 * Returns the type of the token.
	 * @return type of the token
	 */
	public TokenType getType()
	{
		return TYPE;
	}
	
	@Override
	public String toString()
	{
		StringBuilder s = new StringBuilder();
		s.append("['");
		s.append(TYPE.toString());
		if (TYPE != TokenType.NEW_LINE && TYPE != TokenType.TABULATOR && TYPE != TokenType.BLANK)
		{
			s.append("', '");
			s.append(CONTENT.toString());
		}
		s.append("']");
		
		return s.toString();
	}
	
}
