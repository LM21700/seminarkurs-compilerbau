package lukasmaier.compiler.lexer;

import lukasmaier.compiler.errorHandling.ErrorHandler;
import lukasmaier.compiler.lexer.exception.LexerNotYetCreatedException;
import lukasmaier.compiler.lexer.state.State;
import lukasmaier.compiler.lexer.state.States;

/**
 * Represents the first part of the compiler, where the
 * lexicalic analysis takes place <br>
 * 
 * This analysis is managed through the usage of a State-Pattern.
 * @author lukasmaier
 *
 */
public class Lexer 
{
	private static Lexer instance;
	private ErrorHandler errorHandler;
	private final String SOURCE_CODE;
	private TokenStorage tokens;
	private State currentState;
	private int index;
	private StringBuilder currentToken = new StringBuilder();
	
	/**
	 * Creates a new Lexer object. <br>
	 * Only one Lexer object should be exist at the same
	 * time, because some other objects rely on receiving the
	 * reference to the Lexer object through a static variable
	 * and a {@code getInstance()} method.
	 * 
	 * @param sourceCode the source code that should be translated
	 * @param errorHandler the ErrorHandler object that is responsible
	 * for this Lexer object
	 */
	public Lexer(String sourceCode, ErrorHandler errorHandler)
	{
		instance = this;
		this.SOURCE_CODE = sourceCode;
		this.currentState = States.START.getState();
		this.errorHandler = errorHandler;
	}
	
	/**
	 * Returns the instance of the Lexer.
	 * If no Instance exists, it will throw an 
	 * Exception.
	 * 
	 * @return Instance of Lexer, if it exists
	 */
	public static Lexer getInstance()
	{
		if (instance != null)
		{
			return instance;
		} else
		{
			throw new LexerNotYetCreatedException();
		}
	}
	
	/**
	 * Sets the current State of the Lexer State Pattern
	 * to the provided State.
	 * @param s the State that should be now the current state
	 */
	public void setCurrentState(State s)
	{
		currentState = s;
	}
	
	/**
	 * Main function of the Lexer. Starts his operation
	 * @return tokenized source code
	 */
	public TokenStorage tokenize()
	{
		this.index = 0;
		this.tokens = new TokenStorage();
		
		while (index < SOURCE_CODE.length())
		{
			currentState.operate(SOURCE_CODE.charAt(index));
		}
		return tokens;
	}
	
	/**
	 * Goes to the next char of the source code
	 */
	public void nextChar()
	{
		currentToken.append(SOURCE_CODE.charAt(index));
		index++;
	}
	
	/**
	 * returns the char after the current char without
	 * incrementing the index
	 * @return the char that is following of the current char 
	 */
	public char followingChar()
	{
		return SOURCE_CODE.charAt(index + 1); 
	}
	
	/**
	 * adds a token object to the tokenStorage.
	 * Only the TokenType will be provided, as content the
	 * content of the StringBuilder {@code currentToken} will be used.
	 * @param type the type of the token that will be added.
	 */
	public void addToken(TokenType type)
	{
		tokens.addToken(new Token(currentToken.toString(), type));
		currentToken = new StringBuilder();
	}
	
	/**
	 * returns the chars that are stored to make the next token
	 * 
	 * @return the chars, that will make the next token
	 */
	public String getCurrentToken()
	{
		return currentToken.toString();
	}
	
	/**
	 * returns the ErrorHandler that is responsible for
	 * the current Lexer object
	 * 
	 * @return the ErrorHandler
	 */
	public ErrorHandler getErrorHandler()
	{
		return errorHandler;
	}
	
	/**
	 * Returns the line the character which is currently checked
	 * is in.
	 * @return the line in which the current char is in
	 */
	public int getCurrentLine()
	{
		int line = 1;
		for (int i = 0; i <= index; i++)
		{
			if (SOURCE_CODE.charAt(i) == '\n')
			{
				line++;
			}
		}
		return line;
	}
}
