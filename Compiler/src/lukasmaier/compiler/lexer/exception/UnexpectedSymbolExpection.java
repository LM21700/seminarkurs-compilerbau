package lukasmaier.compiler.lexer.exception;

/**
 *  Gets thrown if any Symbol occurs. <br>
 *  Should be no longer used, instead all Lexer
 *  errors should now be handled through ErrorHandler 
 * @author lukasmaier
 *
 */
@Deprecated
public class UnexpectedSymbolExpection extends RuntimeException
{

	private static final long serialVersionUID = -7963011252993237603L;

	public UnexpectedSymbolExpection()
	{
	}

	public UnexpectedSymbolExpection(String arg0)
	{
		super(arg0);
	}

	public UnexpectedSymbolExpection(Throwable arg0)
	{
		super(arg0);
	}

	public UnexpectedSymbolExpection(String arg0, Throwable arg1)
	{
		super(arg0, arg1);
	}

	public UnexpectedSymbolExpection(String arg0, Throwable arg1, boolean arg2, boolean arg3)
	{
		super(arg0, arg1, arg2, arg3);
	}

}
