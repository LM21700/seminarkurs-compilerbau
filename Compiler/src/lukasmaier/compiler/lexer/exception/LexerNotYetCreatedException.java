package lukasmaier.compiler.lexer.exception;

/**
 * Gets thrown if {@code Lexer.getInstance()} is called and
 * no instance of Lexer exists
 * @author lukasmaier
 *
 */
public class LexerNotYetCreatedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public LexerNotYetCreatedException()
	{
		
	}

	public LexerNotYetCreatedException(String arg0) 
	{
		super(arg0);
	}

	public LexerNotYetCreatedException(Throwable arg0) 
	{
		super(arg0);
	}

	public LexerNotYetCreatedException(String arg0, Throwable arg1) 
	{
		super(arg0, arg1);
	}

	public LexerNotYetCreatedException(String arg0, Throwable arg1, boolean arg2, boolean arg3) 
	{
		super(arg0, arg1, arg2, arg3);
	}

}
