package lukasmaier.compiler.semanticAnalysis;

import java.util.ArrayList;

/**
 * Data structure used to store variables that are declared in different
 * functions.
 * @author lukasmaier
 *
 */
public class VariableStorage
{
	private ArrayList<Variable> variables;
	
	/**
	 * Creates a new, empty VariableStorage object
	 */
	public VariableStorage()
	{
		variables = new ArrayList<>();
	}
	
	/**
	 * Adds a variable to the variable storage. <br>
	 * does not check if the provided variable is already
	 * in the variable storage
	 * @param v variable which should be added
	 */
	public void addVariable(Variable v)
	{
		variables.add(v);
	}
	
	/**
	 * Returns the content of the variable storage in form of
	 * an ArrayList filled with variables.
	 * @return content of the variable storage
	 */
	public ArrayList<Variable> getVariables()
	{
		return variables;
	}
	
	/**
	 * Checks if there is a variable in the variable storage
	 * with the same name as the provided name.
	 * @param varName name of the variable which should be checked
	 * @return if variable already contains variable with the same name
	 */
	public boolean containsVariable(String varName)
	{
		for (Variable var : variables)
		{
			if (var.getName() == varName)
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Removes all Variables from the variable storage.
	 */
	public void clear()
	{
		variables = new ArrayList<>();
	}

	public VariableStorage clone()
	{
		VariableStorage newStorage = new VariableStorage();
		
		for (Variable v : variables)
		{
			newStorage.addVariable(v);
		}
		
		return newStorage;
	}
	
}
