package lukasmaier.compiler.semanticAnalysis;

/**
 * Data structure used to store type and name
 * of a variable.<br>
 * The type and the name are final and cannot be changed later.
 * @author lukasmaier
 *
 */
public class Variable
{
	private final String TYPE;
	private final String NAME;
	
	/**
	 * Creates a new object of the type 'Variable'.
	 * Type and name can be changed later
	 * @param TYPE type of the variable
	 * @param NAME name of the variable
	 */
	public Variable(String TYPE, String NAME)
	{
		this.TYPE = TYPE;
		this.NAME = NAME;
	}
	
	/**
	 * Returns the name of the variable.
	 * @return name of the variable
	 */
	public String getName()
	{
		return NAME;
	}
	
	/**
	 * Returns the type of the variable.
	 * @return type of the variable
	 */
	public String getType()
	{
		return TYPE;
	}
	
	@Override
	public String toString()
	{
		return TYPE + " " + NAME;
	}
	
	@Override
	public boolean equals(Object o)
	{
		return (o instanceof Variable && ((Variable) o).getName() == this.getName());
	}
}
