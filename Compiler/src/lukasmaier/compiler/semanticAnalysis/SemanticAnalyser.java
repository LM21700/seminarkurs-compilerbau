package lukasmaier.compiler.semanticAnalysis;

import java.util.ArrayList;

import lukasmaier.compiler.errorHandling.ErrorHandler;
import lukasmaier.compiler.errorHandling.ExitCodes;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElement;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElementType;

// LONGTERM allow only specific variable types (char, int, ...)
// TODO Check existence of all functions before analyzing function blocks
/**
 * Checks if the source code is semantically correct. <br>
 * The source code is provided in form of an abstract syntax tree.
 * @author lukasmaier
 *
 */
public class SemanticAnalyser
{
	private AbstractSyntaxTree ast;
	private ErrorHandler errorHandler;
	private ArrayList<Function> functions;

	/**
	 * Creates a new semantical analyzer object.
	 * @param ast abstract syntax tree which should be analyzed
	 * @param errorHandler error handler
	 */
	public SemanticAnalyser(AbstractSyntaxTree ast, ErrorHandler errorHandler)
	{
		this.ast = ast;
		this.errorHandler = errorHandler;
		this.functions = new ArrayList<>();
		ast.toMainElement();
	}
	
	/**
	 * Analyzes the abstract syntax tree for semantical correctness.
	 */
	public void analyse()
	{
		ArrayList<AbstractSyntaxTreeElement> astElements = ast.getCurrentElement().getFollowingElements();
		for (int i = 0; i < astElements.size(); i++)
		{
			AbstractSyntaxTreeElement currentElement = astElements.get(i);
			if (currentElement.getType() == AbstractSyntaxTreeElementType.IMPORT)
			{
				analyseImport(currentElement);
			} else if (currentElement.getType() == AbstractSyntaxTreeElementType.FUNCTION)
			{
				analyseFunction(currentElement);
			}
		}
	}
	
	// IMPLEMENT analyseImport(AbstractSyntaxTreeElement) in class SemanticAnalysis
	private void analyseImport(AbstractSyntaxTreeElement astElement)
	{
		
	}
	
	private void analyseFunction(AbstractSyntaxTreeElement astElement)
	{
		Function function = new Function(astElement.getFollowingElementOfType(AbstractSyntaxTreeElementType.FUNCTION_NAME).getValue());
		function.setReturnType((astElement.hasFollowingElementOfType(AbstractSyntaxTreeElementType.RETURN_TYPE)) ? astElement.getFollowingElementOfType(AbstractSyntaxTreeElementType.RETURN_TYPE).getValue() : "void");
		if (astElement.hasFollowingElementOfType(AbstractSyntaxTreeElementType.ARGUEMENTS))
		{
			// TODO function.setArguments(analyseArguements(astElement.getFollowingElementOfType(AbstractSyntaxTreeElementType.ARGUEMENTS)));
		}
		
		if (isFunctionAlreadyDefined(function))
		{
			errorHandler.printError("Function exists already:\n" + function.toString(), ExitCodes.SEMANTIC_ANALIZER_ERROR);
		}
		
		analyseBlock(astElement.getFollowingElementOfType(AbstractSyntaxTreeElementType.BLOCK), function.getArguments());
	}
	
	
	private boolean isFunctionAlreadyDefined(Function function)
	{
		for (Function f : functions)
		{
			if (f.equals(function))
			{
				return true;
			}
		}
		return false;
	}
	
	private ArrayList<Variable> analyseArguements(AbstractSyntaxTreeElement astElement)
	{
		ArrayList<Variable> arguements = new ArrayList<>();
		ArrayList<AbstractSyntaxTreeElement> followingElements = astElement.getFollowingElements();
		
		for (AbstractSyntaxTreeElement element : followingElements)
		{
			arguements.add(new Variable(element.getValue(), element.getFollowingElements().get(0).getValue()));
		}
		
		return arguements;
	}
	
	// IMPLEMENT analyseBlock(AbstractSyntaxTreeElement) in class SemanticAnalyser
	private void analyseBlock(AbstractSyntaxTreeElement astElement, VariableStorage existingVariables)
	{
		VariableStorage variables = existingVariables.clone();
		ArrayList<AbstractSyntaxTreeElement> followingElements = astElement.getFollowingElements();
		for (AbstractSyntaxTreeElement current : followingElements)
		{
			if (current.getType() == AbstractSyntaxTreeElementType.VARIABLE_DECLARATION)
			{
				analyseVariableDeclaration(current, variables);
			} else if (current.getType() == AbstractSyntaxTreeElementType.ASSIGNMENT)
			{
				analyseAssignment(current, variables);
			} else
			{
				errorHandler.printError("Not yet implemented", ExitCodes.SEMANTIC_ANALIZER_ERROR);
			}
		}
		
		
	}
	
	private void analyseVariableDeclaration(AbstractSyntaxTreeElement astElement, VariableStorage variables)
	{
		Variable var = new Variable(astElement.getFollowingElementOfType(AbstractSyntaxTreeElementType.VARIBALE_TYPE).getValue(),
				astElement.getFollowingElementOfType(AbstractSyntaxTreeElementType.VARIABLE).getValue());
		
		if (variables.containsVariable(var.getName()))
		{
			errorHandler.printError("Variable '" + var.getName() + "' exists already.", ExitCodes.SEMANTIC_ANALIZER_ERROR);
		}
		
		variables.addVariable(var);
		
	}
	
	// IMPLEMENT analyseAssignment(AbstractSyntaxTreeElement, ArrayList<Variable>) in class SemanticAnalyser
	private void analyseAssignment(AbstractSyntaxTreeElement astElement, VariableStorage existingVariables)
	{
		if (!existingVariables.containsVariable(astElement.getFollowingElements().get(0).getValue()))
		{
			// TODO add Token/... to error message
			errorHandler.printError("Variable does not exist", ExitCodes.SEMANTIC_ANALIZER_ERROR);
		}
		
	}
	
	// IMPLEMENT analyseExpressionAndGetReturnType(AbstractSyntaxTreeElement, VariableStorage) in class SemanticAnalyser
	private String analyseExpressionAndGetReturnType(AbstractSyntaxTreeElement astElement, VariableStorage existingVariables)
	{
		return null;
	}
	
}
