package lukasmaier.compiler.semanticAnalysis;

import java.util.ArrayList;

// TODO use VariableStorage instead of ArrayList to store Variables in class Function

/**
 * Datastructure used to store all Information regarding to a function (name, return type, arguments).
 * @author lukasmaier
 *
 */
public class Function 
{
	private String name;
	private String returnType;
	private VariableStorage arguments;
	
	/**
	 * Creates a new function object.
	 * @param name function name
	 * @param returnType return type of the function
	 * @param arguments the arguments the function has
	 */
	public Function(String name, String returnType, VariableStorage arguments)
	{
		this.name = name;
		this.returnType = returnType;
		this.arguments = (arguments != null) ? arguments : new VariableStorage();
	}
	
	/**
	 * Creates a new function object.
	 * @param name function name
	 * @param returnType return type of the function
	 */
	public Function(String name, String returnType)
	{
		this.name = name;
		this.returnType = returnType;
		this.arguments = new VariableStorage();
	}
	
	/**
	 * Creates a new function object with a function name. The return type
	 * and arguments will be empty
	 * @param name name of the function
	 */
	public Function(String name)
	{
		this(name, "");
	}
	
	/**
	 * Creates a function object without any values.
	 */
	public Function()
	{
		this("");
	}
	
	/**
	 * Sets the name of the function.
	 * @param name name which should be set as function name
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	
	/**
	 * Sets the return type of the function
	 * @param returnType type which should be stored as return type of the function
	 */
	public void setReturnType(String returnType)
	{
		this.returnType = returnType;
	}
	
	/**
	 * Removes all existing arguments and add the provided ones.
	 * @param arguments arguments which should be set
	 */
	public void setArguments(VariableStorage arguments)
	{
		this.arguments = arguments;
	}
	
	/**
	 * Adds an argument to the list of arguments of the function.
	 * @param argument argument which should be added
	 */
	public void addArgument(Variable argument)
	{
		this.arguments.addVariable(argument);
	}
	
	/**
	 * Removes all arguments from the function
	 */
	public void clearArguments()
	{
		this.arguments.clear();
	}
	
	/**
	 * Returns the name of the function
	 * @return name of the function
	 */
	public String getName()
	{
		return this.name;
	}
	
	/**
	 * Returns the return type of the function.
	 * @return return type of the functions
	 */
	public String getReturnType()
	{
		return this.returnType;
	}
	
	/**
	 * Returns the arguments of the function.
	 * @return arguments of the function
	 */
	public VariableStorage getArguments()
	{
		return this.arguments;
	}
	
	public String toString()
	{
		StringBuilder result = new StringBuilder();
		result.append(getName() + "(");
		for (int i = 0; i < arguments.getVariables().size(); i ++)
		{
			result.append(arguments.getVariables().get(i).getType());
			result.append(", ");
		}
		if (result.charAt(result.length() - 1) == ' ' && result.charAt(result.length() - 2) == ',')
		{
			result.delete(result.length() - 2, result.length());
		}
		result.append(")");
		
		return result.toString();
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof Function)
		{
			 Function f2 = (Function) o;
			 if (f2.getArguments().getVariables().size() != arguments.getVariables().size())
			 {
				 return false;
			 }
			 
			 ArrayList<Variable> otherArguements = f2.getArguments().getVariables();
			 for (int i = 0; i < arguments.getVariables().size(); i++)
			 {
				 if (!(otherArguements.get(i).getType() == arguments.getVariables().get(i).getType()))
				 {
					 return false;
				 }
			 }
			 return true;
		}
		return false;
	}
}
