package lukasmaier.compiler.codeGeneration;

import java.util.ArrayList;

import lukasmaier.compiler.errorHandling.ErrorHandler;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTree;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElement;
import lukasmaier.compiler.syntacticAnalysis.ast.AbstractSyntaxTreeElementType;

// LONGTERM indent blocks in final code
/**
 * Uses an abstract syntax tree to create C-code.<br>
 * Abstract syntax tree must be checked for syntactical and 
 * semantical correctness before it can be translated.
 * 
 * @author lukasmaier
 *
 */
public class CodeGenerator 
{
	private AbstractSyntaxTree ast;
	private ErrorHandler errorHandler;
	StringBuilder targetCode;
	
	/**
	 * Creates a new code generator object.
	 * @param ast abstract syntax tree which should be analyzed
	 * @param errorHandler error handler
	 */
	public CodeGenerator(AbstractSyntaxTree ast, ErrorHandler errorHandler)
	{
		this.ast = ast;
		this.errorHandler = errorHandler;
		this.ast.toMainElement();
		this.targetCode = new StringBuilder();
	}
	
	/**
	 * Generates C-code from the abstract syntax tree and returns it as string.
	 * @return C-code
	 */
	public String generate()
	{
		ArrayList<AbstractSyntaxTreeElement> followingToMainElement = ast.getCurrentElement().getFollowingElements();
		for (int i = 0; i < followingToMainElement.size(); i++)
		{
			AbstractSyntaxTreeElement currentElement = followingToMainElement.get(i);
			if (currentElement.getType() == AbstractSyntaxTreeElementType.IMPORT)
			{
				// to ensure that import is at the beginning of the file
				targetCode.reverse().append(new StringBuilder()
						.append("#import " + currentElement.getValue() + "\n").reverse()).reverse();
			} else if (currentElement.getType() == AbstractSyntaxTreeElementType.FUNCTION)
			{
				targetCode.append(translateFunction(currentElement));
			}
		}
				
		return targetCode.toString();
	}
	
	private String translateFunction(AbstractSyntaxTreeElement astElement)
	{
		StringBuilder functionCode = new StringBuilder();
		
		// return type
		if (astElement.hasFollowingElementOfType(AbstractSyntaxTreeElementType.RETURN_TYPE))
		{
			functionCode.append(astElement.getFollowingElementOfType(
					AbstractSyntaxTreeElementType.RETURN_TYPE).getValue());
			functionCode.append(" ");
		} else
		{
			functionCode.append("void ");
		}
		
		// function name
		functionCode.append(astElement.getFollowingElementOfType(AbstractSyntaxTreeElementType.FUNCTION_NAME)
				.getValue());
		
		// opening parenthesis
		functionCode.append("(");
		
		// arguements
		if (astElement.hasFollowingElementOfType(AbstractSyntaxTreeElementType.ARGUEMENTS))
		{
			functionCode.append(translateArguements(astElement.getFollowingElementOfType(AbstractSyntaxTreeElementType.ARGUEMENTS)));
		}
		
		// closing parenthesis
		functionCode.append(")\n");
		
		// opening curly bracket
		functionCode.append("{\n");
		
		// functionBlock
		if (astElement.hasFollowingElementOfType(AbstractSyntaxTreeElementType.BLOCK))
		{
			functionCode.append(translateBlock(astElement.getFollowingElementOfType(AbstractSyntaxTreeElementType.BLOCK)));
		}
		
		// closing curly bracket
		functionCode.append("}\n");
		
		return functionCode.toString();
	}
	
	
	private String translateArguements(AbstractSyntaxTreeElement astElement)
	{
		StringBuilder arguements = new StringBuilder();
		for (int i = 0; i < astElement.getFollowingElements().size(); i++)
		{
			arguements.append(astElement.getFollowingElements().get(i).getValue() + " ");
			arguements.append(astElement.getFollowingElements().get(i).getFollowingElements().get(0).getValue());
			if (i < (astElement.getFollowingElements().size() - 1))
			{
				arguements.append(", ");
			}
		}
		
		return arguements.toString();
	}
	
	// IMPLEMENT translateBlock(AbstractTreeElement) in class CodeGenerator
	private String translateBlock(AbstractSyntaxTreeElement astElement)
	{
		StringBuilder code = new StringBuilder();
		ArrayList<AbstractSyntaxTreeElement> followingElements = astElement.getFollowingElements();
		
		for (AbstractSyntaxTreeElement e : followingElements)
		{
			if (e.getType() == AbstractSyntaxTreeElementType.FUNCTION_CALL)
			{
				
			} else if (e.getType() == AbstractSyntaxTreeElementType.ASSIGNMENT)
			{
				code.append(e.getFollowingElementOfType(AbstractSyntaxTreeElementType.VARIABLE).getValue());
				code.append(" = ");
				code.append(translateExpression(e.getFollowingElements().get(1)));
				code.append(";\n");
			} else if (e.getType() == AbstractSyntaxTreeElementType.VARIABLE_DECLARATION)
			{
				code.append(e.getFollowingElementOfType(AbstractSyntaxTreeElementType.VARIBALE_TYPE).getValue());
				code.append(" ");
				code.append(e.getFollowingElementOfType(AbstractSyntaxTreeElementType.VARIABLE).getValue());
				code.append(";\n");
			}
		}
		
		return code.toString();
	}
	
	// IMPLEMENT translateExpression(AbstractSyntaxTreeElement) in class CodeGenerator
	private String translateExpression(AbstractSyntaxTreeElement astElement)
	{
		StringBuilder code = new StringBuilder();
		if(astElement.getType() == AbstractSyntaxTreeElementType.INTEGER 
				|| astElement.getType() == AbstractSyntaxTreeElementType.FLOAT)
		{
			code.append(astElement.getValue());
		}
		
		return code.toString();
	}
}
