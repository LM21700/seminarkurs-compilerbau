package lukasmaier.util;

/**
 * Utility class for work with chars.
 * @author lukasmaier
 *
 */
public class CharUtil
{
	/**
	 * Checks if provided char is a digit or not
	 * 
	 * @param c character which should be checked
	 * @return whether c is a digit or not
	 */
	public static boolean isDigit(char c)
	{
		return (c == '0' || c == '1' || c == '2' || c == '3' || c == '4' || c == '5' || c == '6' || c== '7' || c == '8' || c == '9')? true: false;
	}
	
	/**
	 * Checks if the provided char is a whitespace character (' ', '\t', '\n') or not
	 * @param c character which should be checked
	 * @return whether c is a whitespace character or not
	 */
	public static boolean isWhitespace(char c)
	{
		return (c == ' ' || c == '\n' || c == '\t');
	}
	
	/**
	 * Checks if the provided char is a parenthesis, bracket or curly bracket
	 * @param c character which should be checked
	 * @return whether c is a parenthesis or not
	 */
	public static boolean isParenthesis(char c)
	{
		return (c == '(' || c == ')' || c == '[' || c == ']' || c == '{' || c == '}');
	}
	
	/**
	 * Checks if the provided char is a arimethic operator or not.
	 * These can be '+', '-', '*', '/' or '%'
	 * @param c character which should be checked
	 * @return whether c is a arimethic or not
	 */
	public static boolean isArimethicOperator(char c)
	{
		return (c == '+' || c == '-' || c == '*' || c == '/' || c == '%');
	}
	
	/**
	 * Checks if the provided char is a letter or not
	 * @param c character which should be checked
	 * @return whether c is a letter or not
	 */
	public static boolean isLetter(char c)
	{
		return Character.isLetter(c);
	}
	
}
