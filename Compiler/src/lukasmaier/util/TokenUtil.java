package lukasmaier.util;

import lukasmaier.compiler.lexer.Token;
import lukasmaier.compiler.lexer.TokenType;

/**
 * Utility class for working with the class Token
 * @author lukasmaier
 *
 */
public class TokenUtil
{
	/**
	 * Checks if provided Token is used for indentation or not
	 * 
	 * @param t Token with should be checked
	 * @return whether the provided Token is used for indentation or not.
	 */
	public static boolean isIndentationCharakter(Token t)
	{
		return (t.getType() == TokenType.BLANK || t.getType() == TokenType.TABULATOR);
	}
}
