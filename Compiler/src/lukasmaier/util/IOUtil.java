package lukasmaier.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Utility class for input/output concerns.
 * @author lukasmaier
 *
 */
public class IOUtil
{
	/**
	 * Reads the content of a file and returns it as a String.
	 * 
	 * @param filename path to the file that should be read
	 * @return content of file
	 * @throws IOException
	 */
	public static String readFile(String filename) throws IOException 
	{
	    BufferedReader r = new BufferedReader(new FileReader (filename));
	    String         line = null;
	    StringBuilder  s = new StringBuilder();

	    try 
	    {
	        while((line = r.readLine()) != null) 
	        {
	            s.append(line);
	            s.append("\n");
	        }

	        return s.toString();
	    } finally 
	    {
	        r.close();
	    }
	}
	
	/**
	 * Writes a String into a file.
	 * Checks not if file already exists.
	 * 
	 * @param filename path of the file where the String should be written
	 * @param content String that should be wrote to the file
	 * @throws IOException
	 */
	public static void writeFile(String filename, String content) throws IOException
	{
		BufferedWriter w = new BufferedWriter(new FileWriter(new File(filename)));
		
		try 
		{
			w.write(content);
		} finally
		{
			w.close();
		}
	}


}
